-- DATABASE
mysql -u root -p awE_M1Y@Xp>xUW(s

-- CREATE USER	
CREATE DATABASE journal DEFAULT CHARSET utf8mb4;

CREATE USER 'journal'@'localhost' IDENTIFIED BY 'awE_M1Y@Xp>xUW(s';

GRANT ALL PRIVILEGES ON journal.* TO 'journal'@'localhost';


-- CREATE TABLE 
-- joboffers
CREATE TABLE IF NOT EXISTS joboffers(
	id INTEGER NOT NULL AUTO_INCREMENT,
	rqmt_id VARCHAR(50) UNIQUE NOT NULL,
	sitename VARCHAR(255) NOT NULL,
	syokushu_Cd TEXT NOT NULL,
	content LONGTEXT,
	workplace_Cd TEXT,
	url VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
)

-- baitoru
CREATE TABLE IF NOT EXISTS tj_scraping_for_bitr(
    id INTEGER NOT NULL AUTO_INCREMENT,
    rqmt_id VARCHAR(50) UNIQUE NOT NULL,
    sitename VARCHAR(255) NOT NULL,
    cmpny_name VARCHAR(255) NOT NULL,
    subtitle TEXT NOT NULL,
    syokushu_Cd VARCHAR(255) NOT NULL,
    content MEDIUMTEXT NOT NULL,
    content_wiz_tag MEDIUMTEXT NOT NULL,
    workplace_Cd VARCHAR(255) NOT NULL,
    workplace MEDIUMTEXT NOT NULL,
    workplace_wiz_tag MEDIUMTEXT NOT NULL,
    skill MEDIUMTEXT NOT NULL,
    skill_wiz_tag MEDIUMTEXT NOT NULL,
    payment MEDIUMTEXT NOT NULL,
    payment_wiz_tag MEDIUMTEXT NOT NULL,
    last_confirmed_at DATE NOT NULL,
    url VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
)DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

-- hatalike
CREATE TABLE IF NOT EXISTS tj_scraping_for_htlk(
    id INTEGER NOT NULL AUTO_INCREMENT,
    rqmt_id VARCHAR(50) UNIQUE NOT NULL,
    sitename VARCHAR(255) NOT NULL,
    cmpny_name VARCHAR(255) NOT NULL,
    subtitle TEXT NOT NULL,
    syokushu_Cd VARCHAR(255) NOT NULL,
    message MEDIUMTEXT,
    content MEDIUMTEXT NOT NULL,
    content_wiz_tag MEDIUMTEXT NOT NULL,
    workplace_Cd VARCHAR(255) NOT NULL,
    workplace MEDIUMTEXT NOT NULL,
    workplace_wiz_tag MEDIUMTEXT NOT NULL,
    skill MEDIUMTEXT NOT NULL,
    skill_wiz_tag MEDIUMTEXT NOT NULL,
    payment MEDIUMTEXT NOT NULL,
    payment_wiz_tag MEDIUMTEXT NOT NULL,
    expired_at DATE,
    last_confirmed_at DATE NOT NULL,
    url VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
)DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;


-- mynabi
CREATE TABLE IF NOT EXISTS tj_scraping_for_mynb(
    id INTEGER NOT NULL AUTO_INCREMENT,
    rqmt_id VARCHAR(50) UNIQUE NOT NULL,
    sitename VARCHAR(255) NOT NULL,
    cmpny_name VARCHAR(255) NOT NULL,
    subtitle TEXT NOT NULL,
    syokushu_Cd VARCHAR(255) NOT NULL,
    content MEDIUMTEXT NOT NULL,
    content_wiz_tag MEDIUMTEXT NOT NULL,
    workplace_Cd VARCHAR(255) NOT NULL,
    workplace TEXT NOT NULL,
    skill TEXT NOT NULL,
    skill_wiz_tag TEXT NOT NULL,
    payment TEXT NOT NULL,
    url VARCHAR(255) NOT NULL,
    last_confirmed_at DATE NOT NULL,
    PRIMARY KEY(id)
)DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;


--next
CREATE TABLE IF NOT EXISTS tj_scraping_for_next(
    id INTEGER NOT NULL AUTO_INCREMENT,
    rqmt_id VARCHAR(50) UNIQUE NOT NULL,
    sitename VARCHAR(255) NOT NULL,
    cmpny_name VARCHAR(255) NOT NULL,
    subtitle TEXT NOT NULL,
    syokushu_Cd VARCHAR(255) NOT NULL,
    message MEDIUMTEXT,
    content MEDIUMTEXT NOT NULL,
    content_wiz_tag MEDIUMTEXT NOT NULL,
    workplace_Cd VARCHAR(255) NOT NULL,
    workplace MEDIUMTEXT NOT NULL,
    workplace_wiz_tag MEDIUMTEXT NOT NULL,
    skill MEDIUMTEXT NOT NULL,
    skill_wiz_tag MEDIUMTEXT NOT NULL,
    payment MEDIUMTEXT NOT NULL,
    payment_wiz_tag MEDIUMTEXT NOT NULL,
    url VARCHAR(255) NOT NULL,
    expired_at DATE,
    last_confirmed_at DATE NOT NULL,
    PRIMARY KEY(id)
)DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;



-- rag
CREATE TABLE IF NOT EXISTS tb_cassettes_for_rag(
    id INTEGER NOT NULL AUTO_INCREMENT,
    rqmt_id VARCHAR(50) UNIQUE NOT NULL,
    sitename VARCHAR(255) NOT NULL,
    cmpny_name VARCHAR(255) NOT NULL,
    subtitle TEXT NOT NULL,
    syokushu_Cd VARCHAR(255) NOT NULL,
    content MEDIUMTEXT NOT NULL,
    content_wiz_tag MEDIUMTEXT NOT NULL,
    workplace_Cd VARCHAR(255) NOT NULL,
    workplace MEDIUMTEXT NOT NULL,
    skill MEDIUMTEXT NOT NULL,
    skill_wiz_tag MEDIUMTEXT NOT NULL,
    payment MEDIUMTEXT NOT NULL,
    url VARCHAR(255) NOT NULL,
    last_confirmed_at DATE NOT NULL,
    PRIMARY KEY(id)
)DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;


-- rikuha
CREATE TABLE IF NOT EXISTS tb_cassettes_for_rkha(
    id INTEGER NOT NULL AUTO_INCREMENT,
    rqmt_id VARCHAR(50) UNIQUE NOT NULL,
    sitename VARCHAR(255) NOT NULL,
    cmpny_name VARCHAR(255) NOT NULL,
    subtitle TEXT NOT NULL,
    syokushu_Cd VARCHAR(255) NOT NULL,
    content MEDIUMTEXT NOT NULL,
    workplace_Cd VARCHAR(255) NOT NULL,
    workplace TEXT NOT NULL,
    skill TEXT NOT NULL,
    payment TEXT NOT NULL,
    url VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
)DEFAULT CHARSET=utf8mb4;


-- tora
CREATE TABLE IF NOT EXISTS tj_scraping_for_tora(
    id INTEGER NOT NULL AUTO_INCREMENT,
    rqmt_id VARCHAR(50) UNIQUE NOT NULL ,
    sitename VARCHAR(255) NOT NULL,
    cmpny_name VARCHAR(255) NOT NULL,
    subtitle TEXT NOT NULL,
    syokushu_Cd VARCHAR(255) NOT NULL,
    content MEDIUMTEXT NOT NULL,
    workplace_Cd VARCHAR(255) NOT NULL,
    workplace TEXT NOT NULL,
    skill TEXT NOT NULL,
    payment TEXT NOT NULL,
    url VARCHAR(255) NOT NULL,
    expired_at DATE,
    last_confirmed_at DATE NOT NULL,
    PRIMARY KEY(id)
)DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

-- type
CREATE TABLE IF NOT EXISTS tj_scraping_for_type(
    id INTEGER NOT NULL AUTO_INCREMENT,
    rqmt_id VARCHAR(50) UNIQUE NOT NULL,
    sitename VARCHAR(255) NOT NULL,
    cmpny_name VARCHAR(255) NOT NULL,
    subtitle TEXT NOT NULL,
    syokushu_Cd VARCHAR(255) NOT NULL,
    message MEDIUMTEXT,
    content MEDIUMTEXT NOT NULL,
    content_wiz_tag MEDIUMTEXT NOT NULL,
    workplace_Cd VARCHAR(255) NOT NULL,
    workplace MEDIUMTEXT NOT NULL,
    workplace_wiz_tag MEDIUMTEXT NOT NULL,
    skill MEDIUMTEXT NOT NULL,
    skill_wiz_tag MEDIUMTEXT NOT NULL,
    payment MEDIUMTEXT NOT NULL,
    payment_wiz_tag MEDIUMTEXT NOT NULL,
    url VARCHAR(255) NOT NULL,
    expired_at DATE,
    last_confirmed_at DATE NOT NULL,
    PRIMARY KEY(id)
)DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

-- workport
CREATE TABLE IF NOT EXISTS tj_scraping_for_wkpt(
    id INTEGER NOT NULL AUTO_INCREMENT,
    rqmt_id VARCHAR(50) UNIQUE NOT NULL,
    sitename VARCHAR(255) NOT NULL,
    cmpny_name VARCHAR(255) NOT NULL,
    subtitle TEXT NOT NULL,
    syokushu_Cd VARCHAR(255) NOT NULL,
    message MEDIUMTEXT,
    content MEDIUMTEXT NOT NULL,
    content_wiz_tag MEDIUMTEXT NOT NULL,
    workplace_Cd VARCHAR(255) NOT NULL,
    workplace MEDIUMTEXT NOT NULL,
    workplace_wiz_tag MEDIUMTEXT NOT NULL,
    skill MEDIUMTEXT NOT NULL,
    skill_wiz_tag MEDIUMTEXT NOT NULL,
    payment MEDIUMTEXT NOT NULL,
    payment_wiz_tag MEDIUMTEXT NOT NULL,
    url VARCHAR(255) NOT NULL,
    last_confirmed_at DATE NOT NULL,
    PRIMARY KEY(id)
)DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

