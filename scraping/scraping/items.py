# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Joboffer(scrapy.Item):
    """
    各サイトのクローリング結果
    """

    #サイト名
    sitename = scrapy.Field()
    #求人ID
    rqmt_id = scrapy.Field()
    #職種CD NOT NULL
    syokushu_Cd = scrapy.Field()
    #勤務地CD
    workplace_Cd = scrapy.Field()
    #URL
    url = scrapy.Field()
    #HTML
    content = scrapy.Field()


class Cassette(scrapy.Item):
    """
    求人票カセット
    """

    #サイト名
    sitename = scrapy.Field()
    #求人ID
    rqmt_id = scrapy.Field()
    #企業名 NOT NULL
    cmpny_name = scrapy.Field()
    #原稿タイトル NOT NULL
    subtitle = scrapy.Field()
    #職種 NOT NULL
    syokushu = scrapy.Field()
    #仕事の内容
    content = scrapy.Field()
    #勤務地（都道府県）
    workplace_pref = scrapy.Field()
    #勤務地（住所）
    workplace = scrapy.Field()
    #会社所在地
    address = scrapy.Field()
    #経験・スキル
    skill = scrapy.Field()
    #給与（年収）
    annual_payment = scrapy.Field()
    #給与（月収）
    monthly_payment = scrapy.Field()
    #URL NOT NULL
    url = scrapy.Field()
