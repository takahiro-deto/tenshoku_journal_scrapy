import csv
import sys
import json
from datetime import datetime

import os
import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

#### code_list作成

big_code_dict = {}
mid_code_dict = {}
area_code_dict = {}

invalid_translate_list = []

import_list = []
import_list_row = []


## code対応表読み込み

with open("job_code_list.csv","r") as f:
    reader = csv.reader(f)

    for row in reader:
        big_code_dict[row[0]] = row[1]
        mid_code_dict[row[0]] = row[2]


with open("area_code_list.csv","r") as f:
    reader = csv.reader(f)

    for row in reader:
        area_code_dict[row[0]] = row[1]


## code変換

with open("dump_cassettes.csv","r") as f:
    reader = csv.reader(f,delimiter='\t')
    big_code_change = False
    mid_code_change = False

    for row in reader:

        for k in big_code_dict:
            if(row[6] == k):
                row[6] = big_code_dict[k]
                big_code_change = True

        for k in mid_code_dict:
            if(row[7] == k):
                row[7] = mid_code_dict[k]
                mid_code_change = True

        if(big_code_change and mid_code_change):
            row[5] = row[6] + row[7]
            big_code_change = False
            mid_code_change = False


        for k in area_code_dict:
            if(row[10] == k):
                row[10] = area_code_dict[k]
                array = list(row[10])
                print(row[10])
                ## convert to json type
                #row[10] = "COLUMN_CREATE('code', %s)" % array
                # row[10] = "{ \"code\" : %s }" % array
                jsonstring = { 'code' : new_area_code_list }
                row[10] = json.dumps(jsonstring)                
        ## mariadb import用csv作成

        with open("import_cassettes.csv","a") as f:
            writer = csv.writer(f, delimiter='\t')
            writer.writerow(row)


if(len(invalid_translate_list) > 0):
    print("INVALID_TRANSLATE_COUNT:" + str(len(invalid_translate_list)))
    today = datetime.now().strftime('%Y-%m-%d')
    filename = "invalid_translate_list_" + today + ".csv"

    with open(filename, 'w') as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerow(['url', 'reason'])

        for array in invalid_translate_list:
            writer.writerow(array)


    ### send mail the first 10 row of csv ###
    # http://robertwdempsey.com/python3-email-with-attachments-using-gmail/
    invalid_translate_list_count = sum(1 for line in filename) - 1 # dif header row

    sender      = 'hello.gdesignlab@gmail.com'
    password    = '@07160227Dt'
    recipient   = 'hello.gdesignlab@gmail.com'
    titletext   = "【完了報告】" + today + "のCODE変換結果"
    body        = today + "のトランスレートが完了しました。" + \
                    "全" + str(invalid_translate_list_count) + \
                    "件のトランスレートエラーがあります。"

    outer = MIMEMultipart()
    outer['Subject'] = titletext
    outer['To'] = recipient
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'


    message = MIMEText(body)
    outer.attach(message)


    attachment = './' + filename

    try:
        with open(attachment, 'rb') as fp:
            msg = MIMEBase('application', "octet-stream")
            msg.set_payload(fp.read())
        encoders.encode_base64(msg)
        msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(filename))
        outer.attach(msg)
    except:
        print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
        raise

    composed = outer.as_string()


    # Send the message via our own SMTP server.
    s = smtplib.SMTP('smtp.gmail.com',587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(sender, password)
    s.sendmail(sender, recipient, composed)
    s.close()



    print("success for sending email!")
