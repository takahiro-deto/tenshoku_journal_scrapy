import mysql.connector
import re
import lxml.html
import csv
import sys
from datetime import datetime

import os
import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart


class ScraperTasks(object):
    invalid_scrape_list = []


    def main(self,sitename):
        today = datetime.now().strftime('%Y-%m-%d')

        ### call scraping method ###
        self.scrape(sitename)


        ### create invalid_scrape_list.csv ###
        if(len(self.invalid_scrape_list) > 0):
            print("INVALID_SCRAPE_COUNT:" + len(self.invalid_scrape_list))
            filename = "invalid_scrape_list_" + today + ".csv"

            with open(filename, 'w') as f:
                writer = csv.writer(f, lineterminator='\n')
                writer.writerow(['url', 'reason'])

                for array in self.invalid_scrape_list:
                    writer.writerow(array)


            ### send mail the first 10 row of csv ###
            # http://robertwdempsey.com/python3-email-with-attachments-using-gmail/
            invalid_scrape_list_count = len(open(filename).readlines()) - 1 # dif header row

            sender      = 'hello.gdesignlab@gmail.com'
            password    = '@07160227Dt'
            recipient   = 'hello.gdesignlab@gmail.com'
            titletext   = "【完了報告】" + today + "のスクレイピング結果"
            body        = today + "のスクレピングが完了しました。" + \
                            "全" + str(invalid_scrape_list_count) + \
                            "件のスクレイピングエラーがあります。"

            outer = MIMEMultipart()
            outer['Subject'] = titletext
            outer['To'] = recipient
            outer['From'] = sender
            outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'


            message = MIMEText(body)
            outer.attach(message)


            attachment = './' + filename

            try:
                with open(attachment, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(filename))
                outer.attach(msg)
            except:
                print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
                raise

            composed = outer.as_string()


            # Send the message via our own SMTP server.
            s = smtplib.SMTP('smtp.gmail.com',587)
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(sender, password)
            s.sendmail(sender, recipient, composed)
            s.close()



            print("success for sending email!")



        print("success for creating csv!")






    def scrape(self,sitename):
        self.scrape_cassette_baitoru()



    def scrape_cassette_baitoru(self):
        """
        バイトルNEXT用原稿Cassette Parse
        """
        conn = mysql.connector.connect(
            host='localhost',
            database='journal',
            user='journal',
            passwd='awE_M1Y@Xp>xUW(s',
            charset='utf8mb4',
        )
        c = conn.cursor()
        
        c.execute('''
            SELECT * FROM joboffers WHERE sitename = "baitoru_next";
        ''')

        for row in c.fetchall():
            html = lxml.html.fromstring(row[4])

            # -- sitename --
            sitename = row[2]
            # -- rqmt_id --
            rqmt_id = row[1]
            # -- syokushu NOT NULL --
            syokushu_Cd = row[3]
            # -- workplace pref --
            workplace_Cd = row[5]
            # -- URL NOT NULL --
            url = row[6]

            # -- company_name NOT NULL --
            cmpny_name = ""
            #cmpny_name = html.xpath('string(//*[@id="main"]/article/div[1]/h1)')
            company_name_css = html.cssselect('#next_outline_area > div.detail-companyInfo > div > div.bg01 > div.pt02 > dl > dd > p')
            if(len(company_name_css) > 0):
                #cmpny_name = lxml.html.tostring(company_name_css[0].text,encoding='utf-8').decode('utf-8')
                cmpny_name = company_name_css[0].text.encode('utf-8').decode('utf-8')
                cmpny_name = self.normalize_spaces(cmpny_name)
                cmpny_name = str(cmpny_name)

            # -- title of job offer NOT NULL --
            subtitle = ""
            #subtitle = html.xpath('string(//*[@id="main"]/article/div[2]/div/div[1]/div[1]/div[1]/h2)').strip()
            subtitle_css = html.cssselect('#next_detail_area > div.detail-detailHeader > div > div > div.pt02 > h2')
            if(len(subtitle_css) > 0):
                #subtitle = lxml.html.tostring(subtitle_css[0].text,encoding='utf-8').decode('utf-8')
                #subtitle = subtitle_css[0].text
                subtitle = subtitle_css[0].text.encode('utf-8').decode('utf-8')
                subtitle = self.normalize_spaces(subtitle)
                subtitle = str(subtitle)
            
            # -- content --
            #content = html.xpath("string(//*[contains(text(), '仕事内容')]/following-sibling::*)").strip()
            content = html.xpath('string(//*[@id="next_detail_area"]/div[@class="detail-recruitInfo"]/div/div[2]/div/div/dl[@class="dl01"]/dd/p)').strip()
            content = self.normalize_spaces(content)
            content = str(content)

            # -- content_wiz_tag --
            #cssselectorlist = html.cssselect('#main > article > div.p-item__inner > div > div:nth-child(1) > div.p-item__job')
            cssselectorlist = html.cssselect('#next_detail_area > div:nth-child(2) > div > div:nth-child(2) > div > div > dl.dl01 > dd > p')
            content_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                content_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                #content_wiz_tag = re.sub('\t','',re.sub('\n','',content_wiz_tag))
                content_wiz_tag = content_wiz_tag.replace('\t','').replace('\n','')
                content_wiz_tag = content_wiz_tag.strip()
                content_wiz_tag = str(content_wiz_tag)

            # -- workplace --
            workplace = html.xpath('string(//*[@id="next_detail_area"]/div[@class="detail-basicInfo"]/div/div[2]/div/div/dl[@class="dl04"]/dd)').strip()
            #workplace = re.sub(r'> 地図', '', workplace)
            workplace = self.normalize_spaces(workplace)
            workplace = str(workplace)

            # -- workplace_wiz_tag --
            #cssselectorlist = html.cssselect('#main > article > div.p-item__inner > div > div:nth-child(2) > table  tr:nth-child(2) > td')
            cssselectorlist = html.cssselect('#next_detail_area > div.detail-basicInfo > div > div:nth-child(2) > div > div > dl.dl04 > dd')
            workplace_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                workplace_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                #workplace_wiz_tag = re.sub('\t','',re.sub('\n','',workplace_wiz_tag))
                workplace_wiz_tag = workplace_wiz_tag.replace('\t','').replace('\n','')
                workplace_wiz_tag = workplace_wiz_tag.strip()
                workplace_wiz_tag = str(workplace_wiz_tag)

            # -- skill --
            #skill = html.xpath("string(//*[@class='p-item__condition']/tr/th[contains(text(),'経験・資格')]/following-sibling::td)").strip()
            skill = html.xpath('string(//*[@id="next_detail_area"]/div[@class="detail-recruitInfo"]/div/div[2]/div/div/dl[@class="dl05"]/dd/p)').strip()
            skill = self.normalize_spaces(skill)
            skill = str(skill)

            # -- skill_wiz_tag --
            #cssselectorlist = html.cssselect('#main > article > div.p-item__inner > div > div:nth-child(2) > table >  tr:nth-child(4) > td')
            cssselectorlist = html.cssselect('#next_detail_area > div:nth-child(2) > div > div:nth-child(2) > div > div > dl.dl05 > dd > p')
            skill_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                skill_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                #skill_wiz_tag = re.sub('\t','',re.sub('\n','',skill_wiz_tag))
                skill_wiz_tag = skill_wiz_tag.replace('\t','').replace('\n','')
                skill_wiz_tag = str(skill_wiz_tag)

            # -- payment --
            #payment = html.xpath("string(//*[@class='p-item__condition']/tr/th[contains(text(),'給与')]/following-sibling::td)").strip()
            payment = html.xpath('string(//*[@id="next_detail_area"]/div[@class="detail-basicInfo"]/div/div[2]/div/div/dl[@class="dl02"]/dd)').strip()
            payment = self.normalize_spaces(payment)
            payment = str(payment)

            # -- payment_wiz_tag --
            #cssselectorlist = html.cssselect('#main > article > div.p-item__inner > div > div:nth-child(2) > table  tr:nth-child(1) > td')
            cssselectorlist = html.cssselect('#next_detail_area > div.detail-basicInfo > div > div:nth-child(2) > div > div > dl.dl02 > dd > div')
            payment_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                payment_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                #payment_wiz_tag = re.sub('\t','',re.sub('\n','',payment_wiz_tag))
                payment_wiz_tag = payment_wiz_tag.replace('\t','').replace('\n','')
                payment_wiz_tag = payment_wiz_tag.strip()
                payment_wiz_tag = str(payment_wiz_tag)

            last_confirmed_at = datetime.now().strftime('20%y-%m-%d')

            item = {}
            item['sitename']          = sitename
            item['rqmt_id']           = rqmt_id
            item['cmpny_name']        = cmpny_name
            item['subtitle']          = subtitle
            item['syokushu_Cd']       = syokushu_Cd
            item['content']           = content
            item['content_wiz_tag']   = content_wiz_tag
            item['workplace_Cd']      = workplace_Cd
            item['workplace']         = workplace
            item['workplace_wiz_tag'] = workplace_wiz_tag
            item['skill']             = skill
            item['skill_wiz_tag']     = skill_wiz_tag
            item['payment']           = payment
            item['payment_wiz_tag']   = payment_wiz_tag
            item['url']               = url
            item['last_confirmed_at'] = last_confirmed_at


            self.validate(item)


            create_sql = """
            CREATE TABLE IF NOT EXISTS tj_scraping_for_bitr(
                id INTEGER NOT NULL AUTO_INCREMENT,
                rqmt_id VARCHAR(50) UNIQUE NOT NULL,
                sitename VARCHAR(255) NOT NULL,
                cmpny_name VARCHAR(255) NOT NULL,
                subtitle TEXT NOT NULL,
                syokushu_Cd VARCHAR(255) NOT NULL,
                content MEDIUMTEXT NOT NULL,
                content_wiz_tag MEDIUMTEXT NOT NULL,
                workplace_Cd VARCHAR(255) NOT NULL,
                workplace MEDIUMTEXT NOT NULL,
                workplace_wiz_tag MEDIUMTEXT NOT NULL,
                skill MEDIUMTEXT NOT NULL,
                skill_wiz_tag MEDIUMTEXT NOT NULL,
                payment MEDIUMTEXT NOT NULL,
                payment_wiz_tag MEDIUMTEXT NOT NULL,
                last_confirmed_at DATE NOT NULL,
                url VARCHAR(255) NOT NULL,
                PRIMARY KEY(id)
            )DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;
            """

           # c.execute(create_sql)

            insert_sql = """
            INSERT IGNORE INTO tj_scraping_for_bitr
            (rqmt_id, sitename, cmpny_name, subtitle, syokushu_Cd, content,
            content_wiz_tag, workplace_Cd, workplace, workplace_wiz_tag, skill, skill_wiz_tag, payment, payment_wiz_tag, last_confirmed_at, url)
            VALUES
            (%(rqmt_id)s, %(sitename)s, %(cmpny_name)s, %(subtitle)s, %(syokushu_Cd)s, %(content)s,
            %(content_wiz_tag)s, %(workplace_Cd)s, %(workplace)s, %(workplace_wiz_tag)s, %(skill)s,
            %(skill_wiz_tag)s, %(payment)s, %(payment_wiz_tag)s, %(last_confirmed_at)s, %(url)s)
            """

            c.execute(insert_sql,item)
            conn.commit()


    def normalize_spaces(self, s):
        s = re.sub(r'\s+', " ", s)
        return s


    def validate(self, item):
        if(len(item['cmpny_name']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'cmpny_name is invalid'])
            
        if(len(item['subtitle']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'subtitle is invalid'])
            
        if(len(item['content']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'content is invalid'])
            
        if(len(item['workplace']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'workplace is invalid'])
            
        if(len(item['skill']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'skill is invalid'])
            
        if(len(item['payment']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'payment is invalid'])
            

if __name__ == "__main__":
    argvs = sys.argv
    exe = ScraperTasks()
    exe.main(argvs[1])
