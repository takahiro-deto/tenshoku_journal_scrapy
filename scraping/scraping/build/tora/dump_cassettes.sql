SELECT DISTINCT
  CONCAT('"', id, '"') AS id,
  CONCAT('"', rqmt_id, '"') AS rqmt_id,
  CONCAT('"', sitename, '"') AS sitename,
  CONCAT('"', cmpny_name, '"') AS cmpny_name,
  CONCAT('"', subtitle, '"') AS subtitle,
  CONCAT('"', syokushu_Cd, '"') AS job_code_full,
  CONCAT('"', syokushu_Cd, '"') AS job_code_big,
  CONCAT('"', syokushu_Cd, '"') AS job_code_mid,
  CONCAT('"', content, '"') AS content,
  CONCAT('"', workplace_Cd, '"') AS area_code,
  CONCAT('"', workplace, '"') AS workplace,
  CONCAT('"', skill, '"') AS skill,
  CONCAT('"', payment, '"') AS payment,
  CONCAT('"', url, '"') AS url,
  CONCAT('"', expired_at, '"') AS expired_at,
  CONCAT('"', last_confirmed_at, '"') AS last_confirmed_at
FROM tj_scraping_for_tora;
