SELECT DISTINCT
  CONCAT('"', id, '"') AS id,
  CONCAT('"', rqmt_id, '"') AS rqmt_id,
  CONCAT('"', sitename, '"') AS sitename,
  CONCAT('"', cmpny_name, '"') AS cmpny_name,
  CONCAT('"', subtitle, '"') AS subtitle,
  CONCAT('"', syokushu_Cd, '"') AS syokushu_Cd_big,
  CONCAT('"', syokushu_Cd, '"') AS syokushu_Cd_mid,
  CONCAT('"', content, '"') AS content,
  CONCAT('"', workplace_Cd, '"') AS workplace_Cd,
  CONCAT('"', workplace, '"') AS workplace,
  CONCAT('"', skill, '"') AS skill,
  CONCAT('"', payment, '"') AS payment,
  CONCAT('"', url, '"') AS url
FROM tb_cassettes_for_rkha;

