-- CREATE TABLE IF NOT EXISTS tj_recruitments(
--   id INTEGER NOT NULL AUTO_INCREMENT,
--   rqmt_id VARCHAR(50) UNIQUE NOT NULL,
--   sitename VARCHAR(255) NOT NULL,
--   cmpny_name VARCHAR(255) NOT NULL,
--   subtitle TEXT NOT NULL,
--   job_code_full VARCHAR(255) NOT NULL,
--   job_code_big VARCHAR(255) NOT NULL,
--   job_code_mid VARCHAR(255) NOT NULL,
--   message MEDIUMTEXT,
--   content MEDIUMTEXT NOT NULL,
--   content_wiz_tag MEDIUMTEXT NOT NULL,
--   area_code VARCHAR(255) NOT NULL,
--   workplace TEXT NOT NULL,
--   workplace_wiz_tag TEXT NOT NULL,
--   skill TEXT NOT NULL,
--   skill_wiz_tag TEXT NOT NULL,
--   payment TEXT NOT NULL,
--   payment_wiz_tag TEXT NOT NULL,
--   url VARCHAR(255) NOT NULL,
--   expired_at DATETIME,
--   last_confirmed_at DATETIME,
--   PRIMARY KEY(id)
-- )DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

LOAD DATA LOCAL INFILE './import_cassettes.csv' INTO TABLE tj_recruitments FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\r\n'\
IGNORE 1 LINES \
(@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21) \
SET rqmt_id=@2, sitename=@3, cmpny_name=@4, subtitle=@5, job_code_full=@6, job_code_big=@7, job_code_mid=@8, message=@9, content=@10, content_wiz_tag=@11,\
area_code=@12, workplace=@13, workplace_wiz_tag=@14, skill=@15, skill_wiz_tag=@16, payment=@17, payment_wiz_tag=@18, url=@19, expired_at=@20, last_confirmed_at=@21;
