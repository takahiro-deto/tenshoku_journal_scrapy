import mysql.connector
import re
import lxml.html
import csv
import sys
from datetime import datetime

import os
import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart


class ScraperTasks(object):
    invalid_scrape_list = []

    def main(self,sitename):
        today = datetime.now().strftime('%Y-%m-%d')

        ### call scraping method ###
        self.scrape(sitename)


        ### create invalid_scrape_list.csv ###
        if(len(self.invalid_scrape_list) > 0):
            print("INVLALID_COUNT: " + str(len(self.invalid_scrape_list)))
            filename = "invalid_scrape_list_" + today + ".csv"

            with open(filename, 'w') as f:
                writer = csv.writer(f, lineterminator='\n')
                writer.writerow(['url', 'reason'])

                for array in self.invalid_scrape_list:
                    writer.writerow(array)


            ### send mail the first 10 row of csv ###
            # http://robertwdempsey.com/python3-email-with-attachments-using-gmail/
            invalid_scrape_list_count = len(open(filename).readlines()) - 1  # dif header row

            sender      = 'hello.gdesignlab@gmail.com'
            password    = '@07160227Dt'
            recipient   = 'hello.gdesignlab@gmail.com'
            titletext   = "【完了報告】" + today + "のスクレイピング結果"
            body        = today + "のスクレピングが完了しました。" + \
                            "全" + str(invalid_scrape_list_count) + \
                            "件のスクレイピングエラーがあります。"

            outer = MIMEMultipart()
            outer['Subject'] = titletext
            outer['To'] = recipient
            outer['From'] = sender
            outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'


            message = MIMEText(body)
            outer.attach(message)


            attachment = './' + filename

            try:
                with open(attachment, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(filename))
                outer.attach(msg)
            except:
                print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
                raise

            composed = outer.as_string()


            # Send the message via our own SMTP server.
            s = smtplib.SMTP('smtp.gmail.com',587)
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(sender, password)
            s.sendmail(sender, recipient, composed)
            s.close()



            print("success for sending email!")



        print("success for creating csv!")



    def scrape(self,sitename):
        self.scrape_cassette_rikuha()

    def scrape_cassette_rikuha(self):
        """
        リクナビ派遣用原稿Cassette Parse
        """
        conn = mysql.connector.connect(
            host='localhost',
            db='journal',
            user='journal',
            passwd='awE_M1Y@Xp>xUW(s',
            charset='utf8',
        )
        c = conn.cursor()
        c.execute('''
            SELECT * FROM joboffers WHERE sitename = "rikuha";
        ''')

        for row in c.fetchall():
            html = lxml.html.fromstring(row[4])

            if(html.xpath("//*[@class='txt_noresult_wrap']")):
                pass
            else:
                # -- sitename --
                sitename = row[2]
                # -- rqmt_id --
                rqmt_id = row[1]
                # -- syokushu NOT NULL --
                syokushu_Cd = row[3]
                # -- workplace pref --
                workplace_Cd = row[5]
                # -- URL NOT NULL --
                url = row[6]

                print(sitename + " " + rqmt_id + " " + syokushu_Cd + " " + workplace_Cd + " " + url)

                # -- company_name NOT NULL --
                cmpny_name = "社名非公開"

                # -- title of job offer NOT NULL --
                subtitle = html.xpath('string(//div[@class="cst_dtl_wrap"]/div[@class="cst_dtl_hdr_wrap"]/p[@class="ttl_cst"])')
                subtitle = self.normalize_spaces(subtitle)

                print("subtitle: " + subtitle)

                # -- debuging: subtitle = subtitle[:30] if(len(subtitle)) > 30 else subtitle
                # -- content --
                content = html.xpath("string(//*[@class='txt_job_dtl ph20'])").strip()
                content = self.normalize_spaces(content)
                print("content: " + content)

                # -- debuging: content = content[:30] if(len(content)) > 30 else content
                # -- workplace --
                workplace = html.xpath("string(//*[@class='tbl_cont_cst_dtl_item']/tr/th[contains(text(), '勤務地')]/following-sibling::td)").strip()
                workplace = self.normalize_spaces(workplace)
                print("workplace: " + workplace)

                # -- debuging: workplace = workplace[:30] if(len(workplace)) > 30 else workplace
                # -- skill --
                skill = html.xpath("string(//*[@class='tbl_cont_cst_dtl_item']/tr/th[contains(text(), '対象となる方')]/following-sibling::td)").strip()
                skill = self.normalize_spaces(skill)
                print("skill: " + skill)

                # -- debuging: skill = skill[:30] if(len(skill)) > 30 else skill
                # -- payment --
                payment = "時給：" + html.xpath("string(//*[@class='tbl_cont_cst_dtl_item']/tr/th[contains(text(), '時給')]/following-sibling::td)").strip()
                payment = self.normalize_spaces(payment)
                print("payment: " + payment)

                # -- debuging: payment = payment[:30] if(len(payment)) > 30 else payment

                item = {}
                item['sitename'] = sitename
                item['rqmt_id'] = rqmt_id
                item['cmpny_name'] = cmpny_name
                item['subtitle'] = subtitle
                item['syokushu_Cd'] = syokushu_Cd
                item['content'] = content
                item['workplace_Cd'] = workplace_Cd
                item['workplace'] = workplace
                item['skill'] = skill
                item['payment'] = payment
                item['url'] = url

                self.validate(item)

                create_sql = """
                CREATE TABLE IF NOT EXISTS tb_cassettes_for_rkha(
                    id INTEGER NOT NULL AUTO_INCREMENT,
                    rqmt_id VARCHAR(50) UNIQUE NOT NULL,
                    sitename VARCHAR(255) NOT NULL,
                    cmpny_name VARCHAR(255) NOT NULL,
                    subtitle TEXT NOT NULL,
                    syokushu_Cd VARCHAR(255) NOT NULL,
                    content MEDIUMTEXT NOT NULL,
                    workplace_Cd VARCHAR(255) NOT NULL,
                    workplace TEXT NOT NULL,
                    skill TEXT NOT NULL,
                    payment TEXT NOT NULL,
                    url VARCHAR(255) NOT NULL,
                    PRIMARY KEY(id)
                )DEFAULT CHARSET=utf8mb4;
                """

            #    c.execute(create_sql)

                insert_sql = """
                INSERT IGNORE INTO tb_cassettes_for_rkha
                (rqmt_id, sitename, cmpny_name, subtitle, syokushu_Cd, content, workplace_Cd, workplace, skill, payment, url)
                VALUES
                (%(rqmt_id)s, %(sitename)s, %(cmpny_name)s, %(subtitle)s, %(syokushu_Cd)s, %(content)s, %(workplace_Cd)s, %(workplace)s, %(skill)s, %(payment)s, %(url)s)
                """

                c.execute(insert_sql,item)
                conn.commit()
    def normalize_spaces(self, s):
        s = re.sub(r'\s+', " ", s)
        return s


    def validate(self, item):
        if(len(item['cmpny_name']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'cmpny_name is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['subtitle']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'subtitle is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['content']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'content is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['workplace']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'workplace is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['skill']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'skill is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['payment']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'payment is invalid'])
            #print(self.invalid_scrape_list)


if __name__ == "__main__":
    argvs = sys.argv
    exe = ScraperTasks()
    exe.main(argvs[1])

