#!/bin/bash

PATH=/home/vagrant/.pyenv/shims:/home/vagrant/.pyenv/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/vagrant/.local/bin:/home/vagrant/bin

echo -e "\n\n"
echo "***************START BUILD***************"
# 1. scraping base data
echo -e "# 1. Scraping crawled data..."
python scraper_tasks.py workport

# 2. dump sqlfile and convert into csv
echo -e "\n\n"
echo -e "# 2. Dump SQLfile and convert into CSV..."
# mysql -u tj_user_0001 -p'awE_M1Y@Xp>xUW(s' -h localhost tenshoku_journal \
# -e "`cat dump_cassettes.sql`" | sed -e 's/\t/,/g' > /vagrant/scraping/scraping/build/workport/dump_cassettes.csv

mysql -u tj_user_0001 -p'awE_M1Y@Xp>xUW(s' -h localhost tenshoku_journal -e "`cat dump_cassettes.sql`" > ./dump_cassettes.csv


# 3. translate code_list
echo -e "\n\n"
echo -e "# 3. Translate CODE_LIST..."
python translate.py

# 4. import to  mariadb
echo -e "\n\n"
echo -e "# 4. Importing to MariaDB..."
mysql -u tj_user_0001 -p'awE_M1Y@Xp>xUW(s' --local_infile=1 tenshoku_journal -e "`cat import_mariadb.sql`"

# 5. remove unnecessary file
echo -e "\n\n"
echo -e "# 5. Removing unnecessary file..."
DATE=`date '+%Y_%m_%d_%H%M%S'`
mv dump_cassettes.csv log/dump_cassettes_$DATE.csv
mv import_cassettes.csv log/import_cassettes_$DATE.csv

echo -e "\n\n"
echo "*************FINISHED BUILD!*************"
