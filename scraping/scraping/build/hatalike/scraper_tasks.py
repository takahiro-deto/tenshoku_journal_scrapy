import mysql.connector
import re
import lxml.html
import csv
import sys
from datetime import datetime

import os
import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart


class ScraperTasks(object):
    invalid_scrape_list = []


    def main(self,sitename):
        today = datetime.now().strftime('%Y-%m-%d')

        ### call scraping method ###
        self.scrape(sitename)


        ### create invalid_scrape_list.csv ###
        if(len(self.invalid_scrape_list) > 0):
            filename = "invalid_scrape_list_" + today + ".csv"

            with open(filename, 'w') as f:
                writer = csv.writer(f, lineterminator='\n')
                writer.writerow(['url', 'reason'])

                for array in self.invalid_scrape_list:
                    writer.writerow(array)


            ### send mail the first 10 row of csv ###
            # http://robertwdempsey.com/python3-email-with-attachments-using-gmail/
            invalid_scrape_list_count = len(open(filename).readlines()) - 1 # dif header row

            sender      = 'hello.gdesignlab@gmail.com'
            password    = '@07160227Dt'
            recipient   = 'hello.gdesignlab@gmail.com'
            titletext   = "【完了報告】" + today + "のスクレイピング結果"
            body        = today + "のスクレピングが完了しました。" + \
                            "全" + str(invalid_scrape_list_count) + \
                            "件のスクレイピングエラーがあります。"

            outer = MIMEMultipart()
            outer['Subject'] = titletext
            outer['To'] = recipient
            outer['From'] = sender
            outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'


            message = MIMEText(body)
            outer.attach(message)


            attachment = './' + filename

            try:
                with open(attachment, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(filename))
                outer.attach(msg)
            except:
                print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
                raise

            composed = outer.as_string()


            # Send the message via our own SMTP server.
            s = smtplib.SMTP('smtp.gmail.com',587)
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(sender, password)
            s.sendmail(sender, recipient, composed)
            s.close()



            print("success for sending email!")



        print("success for creating csv!")


    def scrape(self,sitename):
        self.scrape_cassette_hatalike()



    def scrape_cassette_hatalike(self):
        """
        はたらいく用原稿Cassette Parse
        """
        conn = mysql.connector.connect(
            host='localhost',
            db='journal',
            user='journal',
            passwd='awE_M1Y@Xp>xUW(s',
            charset='utf8mb4',
        )
        c = conn.cursor()
        # c = self.get_connection()
        c.execute('''
            SELECT * FROM joboffers WHERE sitename = "hatalike";
        ''')


        for row in c.fetchall():
            html = lxml.html.fromstring(row[4])

            # -- sitename --
            sitename = row[2]
            # -- rqmt_id --
            rqmt_id = row[1]
            #syokushu NOT NULL
            syokushu_Cd = row[3].split(',')[0]
            # -- workplace pref --
            workplace_Cd = row[5]
            # -- URL NOT NULL --
            url = row[6]

            # -- company_name NOT NUL --
            cmpny_name = html.xpath('string(//*[@class="wideFrame"]/div[1]/div/h2/span)')
            cmpny_name = str(cmpny_name)
            # -- debuging: cmpny_name = cmpny_name[:30] if(len(cmpny_name)) > 30 else cmpny_name
            # -- title of job offer NOT NULL --
            subtitle = html.xpath('string(//*[@id="like_status"]/div[1]/div/div[1]/p)').strip()
            subtitle = self.normalize_spaces(subtitle)
            subtitle = str(subtitle)
            # -- debuging: subtitle = subtitle[:30] if(len(subtitle)) > 30 else subtitle
            # -- content --
            content = html.xpath("string(//*[@class='design01']/tr/th[contains(text(), '職種/仕事内容')]/following-sibling::td)").strip()
            content = self.normalize_spaces(content)
            content = str(content)

            # -- debuging: content = content[:30] if(len(content)) > 30 else content
            # -- content_wiz_tag --
            cssselectorlist = html.cssselect('table tr th:contains("職種/仕事内容") + td')
            content_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                content_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                content_wiz_tag = self.normalize_spaces(content_wiz_tag)
                content_wiz_tag = re.sub(r'<td>','<p>',content_wiz_tag)
                content_wiz_tag = re.sub(r'</td>','</p>',content_wiz_tag)
                content_wiz_tag = str(content_wiz_tag)

            # -- message --
            cssselectorlist = html.cssselect('#like_status > div.like_topic_list.like_green > table:nth-child(3) tr:nth-child(1) > td > div > div > dl')
            section1 = ""
            if(len(cssselectorlist) >= 1):
                section1 = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                section1 = self.normalize_spaces(section1)
                section1 = re.sub(r'<dt>|<dd>','<p>',section1)
                section1 = re.sub(r'</dt>|</dd>','</p>',section1)
                section1 = re.sub(r'<dl.*?>|</dl>','',section1)

            cssselectorlist = html.cssselect('#like_status > div.like_topic_list.like_green > table:nth-child(4) tr:nth-child(1) > td > div > div > dl')
            section2 = ""
            if(len(cssselectorlist) >= 1):
                section2 = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                section2 = self.normalize_spaces(section2)
                section2 = re.sub(r'<dt>|<dd>','<p>',section2)
                section2 = re.sub(r'</dt>|</dd>','</p>',section2)
                section2 = re.sub(r'<dl.*?>|</dl>','',section2)

            cssselectorlist = html.cssselect('#like_status > div.like_topic_list.like_green > table:nth-child(5) tr:nth-child(1) > td > div > div > dl')
            section3 = ""
            if(len(cssselectorlist) >= 1):
                section3 = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                section3 = self.normalize_spaces(section3)
                section3 = re.sub(r'<dt>|<dd>','<p>',section3)
                section3 = re.sub(r'</dt>|</dd>','</p>',section3)
                section3 = re.sub(r'<dl.*?>|</dl>','',section3)

            message = section1 + section2 + section3
            message = str(message)

            # -- workplace --
            workplace = html.xpath("string(//*[@class='design01 mt10']/tr/th[contains(text(), '勤務地')]/following-sibling::td)").strip()
            workplace = re.sub(r'> 地図', '', workplace)
            workplace = self.normalize_spaces(workplace)
            workplace = str(workplace)
            # -- debuging: workplace = workplace[:30] if(len(workplace)) > 30 else workplace
            # -- workplace_wiz_tag --
            cssselectorlist = html.cssselect('table tr th:contains("勤務地") + td')
            workplace_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                workplace_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding="utf-8").decode('utf-8')
                workplace_wiz_tag = self.normalize_spaces(workplace_wiz_tag)
                workplace_wiz_tag = re.sub(r'<td>','<p>',workplace_wiz_tag)
                workplace_wiz_tag = re.sub(r'</td>','</p>',workplace_wiz_tag)

            # -- skill --
            skill = html.xpath("string(//*[@class='design01 mt10']/tr/th[contains(text(), '対象となる方')]/following-sibling::td)").strip()
            skill = self.normalize_spaces(skill)
            skill = str(skill)
            # -- debuging: skill = skill[:30] if(len(skill)) > 30 else skill
            # -- skille_wiz_tag --
            cssselectorlist = html.cssselect('table tr th:contains("対象となる方") + td')
            skill_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                skill_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding="utf-8").decode('utf-8')
                skill_wiz_tag = self.normalize_spaces(skill_wiz_tag)
                skill_wiz_tag = re.sub(r'<td>','<p>',skill_wiz_tag)
                skill_wiz_tag = re.sub(r'</td>','</p>',skill_wiz_tag)
                skill_wiz_tag = str(skill_wiz_tag)

            # -- payment monthly --
            payment = html.xpath("string(//*[@class='design01 mt10']/tr/th[contains(text(), '給与')]/following-sibling::td)").strip()
            payment = self.normalize_spaces(payment)
            payment = str(payment)
            # -- debuging: payment = payment[:30] if(len(payment)) > 30 else payment
            # -- payment_wiz_tag --
            cssselectorlist = html.cssselect('table tr th:contains("給与") + td')
            payment_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                payment_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                payment_wiz_tag = self.normalize_spaces(payment_wiz_tag)
                payment_wiz_tag = re.sub(r'<td>','<p>',payment_wiz_tag)
                payment_wiz_tag = re.sub(r'</td>','</p>',payment_wiz_tag)
                payment_wiz_tag = str(payment_wiz_tag)


            # expired_at = html.cssselect('#like_status > div.like_topic_list.like_green > div.like_topic > p > span.bold')[0].text
            # if(expired_at is not None):
            #     expired_at = re.search(r'\d+/\d+/\d+\xa0\d+:\d+',expired_at).group(0)[:10]
            #     expired_at = re.sub(r'/','-',expired_at)
            # else:
            #     expired_at = ""


            last_confirmed_at = datetime.now().strftime('20%y-%m-%d')




            item = {}
            item['sitename'] = sitename
            item['rqmt_id'] = rqmt_id
            item['cmpny_name'] = cmpny_name
            item['subtitle'] = subtitle
            item['syokushu_Cd'] = syokushu_Cd
            item['message'] = message
            item['content'] = content
            item['content_wiz_tag'] = content_wiz_tag
            item['workplace_Cd'] = workplace_Cd
            item['workplace'] = workplace
            item['workplace_wiz_tag'] = workplace_wiz_tag
            item['skill'] = skill
            item['skill_wiz_tag'] = skill_wiz_tag
            item['payment'] = payment
            item['payment_wiz_tag'] = payment_wiz_tag
            item['url'] = url
            # item['expired_at'] = expired_at
            item['last_confirmed_at'] = last_confirmed_at


            self.validate(item)


            create_sql = """
            CREATE TABLE IF NOT EXISTS tj_scraping_for_htlk(
                id INTEGER NOT NULL AUTO_INCREMENT,
                rqmt_id VARCHAR(50) UNIQUE NOT NULL,
                sitename VARCHAR(255) NOT NULL,
                cmpny_name VARCHAR(255) NOT NULL,
                subtitle TEXT NOT NULL,
                syokushu_Cd VARCHAR(255) NOT NULL,
                message MEDIUMTEXT,
                content MEDIUMTEXT NOT NULL,
                content_wiz_tag MEDIUMTEXT NOT NULL,
                workplace_Cd VARCHAR(255) NOT NULL,
                workplace MEDIUMTEXT NOT NULL,
                workplace_wiz_tag MEDIUMTEXT NOT NULL,
                skill MEDIUMTEXT NOT NULL,
                skill_wiz_tag MEDIUMTEXT NOT NULL,
                payment MEDIUMTEXT NOT NULL,
                payment_wiz_tag MEDIUMTEXT NOT NULL,
                expired_at DATE,
                last_confirmed_at DATE NOT NULL,
                url VARCHAR(255) NOT NULL,
                PRIMARY KEY(id)
            )DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;
            """

           # c.execute(create_sql)

            insert_sql = """
            INSERT IGNORE INTO tj_scraping_for_htlk
            (rqmt_id, sitename, cmpny_name, subtitle, syokushu_Cd, message, content,
            content_wiz_tag, workplace_Cd, workplace, workplace_wiz_tag, skill, skill_wiz_tag, payment, payment_wiz_tag, url, expired_at, last_confirmed_at)
            VALUES
            (%(rqmt_id)s, %(sitename)s, %(cmpny_name)s, %(subtitle)s, %(syokushu_Cd)s, %(message)s, %(content)s,
            %(content_wiz_tag)s, %(workplace_Cd)s, %(workplace)s, %(workplace_wiz_tag)s, %(skill)s,
            %(skill_wiz_tag)s, %(payment)s, %(payment_wiz_tag)s, %(url)s, NULL, %(last_confirmed_at)s )
            """

            c.execute(insert_sql,item)
            conn.commit()




    def normalize_spaces(self, s):
        s = re.sub(r'\s+', " ", s)
        return s


    def validate(self, item):
        if(len(item['cmpny_name']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'cmpny_name is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['subtitle']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'subtitle is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['content']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'content is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['workplace']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'workplace is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['skill']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'skill is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['payment']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'payment is invalid'])
            #print(self.invalid_scrape_list)




if __name__ == "__main__":
    argvs = sys.argv
    exe = ScraperTasks()
    exe.main(argvs[1])
