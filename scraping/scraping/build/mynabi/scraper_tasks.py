import mysql.connector
import re
import lxml.html
import csv
import sys
from datetime import datetime

import os
import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart


class ScraperTasks(object):
    invalid_scrape_list = []


    def main(self,sitename):
        today = datetime.now().strftime('%Y-%m-%d')

        ### call scraping method ###
        self.scrape(sitename)


        ### create invalid_scrape_list.csv ###
        if(len(self.invalid_scrape_list) > 0):
            print("INVALID_SCRAPE_COUNTS: " + str(len(self.invalid_scrape_list)))
            filename = "invalid_scrape_list_" + today + ".csv"

            with open(filename, 'w') as f:
                writer = csv.writer(f, lineterminator='\n')
                writer.writerow(['url', 'reason'])

                for array in self.invalid_scrape_list:
                    writer.writerow(array)


            ### send mail the first 10 row of csv ###
            # http://robertwdempsey.com/python3-email-with-attachments-using-gmail/
            invalid_scrape_list_count = len(open(filename).readlines()) - 1 # dif header row

            sender      = 'hello.gdesignlab@gmail.com'
            password    = '@07160227Dt'
            recipient   = 'hello.gdesignlab@gmail.com'
            titletext   = "【完了報告】" + today + "のスクレイピング結果"
            body        = today + "のスクレピングが完了しました。" + \
                            "全" + str(invalid_scrape_list_count) + \
                            "件のスクレイピングエラーがあります。"

            outer = MIMEMultipart()
            outer['Subject'] = titletext
            outer['To'] = recipient
            outer['From'] = sender
            outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'


            message = MIMEText(body)
            outer.attach(message)


            attachment = './' + filename

            try:
                with open(attachment, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(filename))
                outer.attach(msg)
            except:
                print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
                raise

            composed = outer.as_string()


            # Send the message via our own SMTP server.
            s = smtplib.SMTP('smtp.gmail.com',587)
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(sender, password)
            s.sendmail(sender, recipient, composed)
            s.close()



            print("success for sending email!")



        print("success for creating csv!")



    def scrape(self,sitename):
        self.scrape_cassette_mynabi()



    def scrape_cassette_mynabi(self):
        """
        マイナビエージェント用原稿Cassette Parse
        """
        conn = mysql.connector.connect(
            host='localhost',
            db='journal',
            user='journal',
            passwd='awE_M1Y@Xp>xUW(s',
            charset='utf8',
        )
        c = conn.cursor()
        # c = self.get_connection()
        c.execute('''
            SELECT * FROM joboffers WHERE sitename = "mynabi_agent";
        ''')

        for row in c.fetchall():
            html = lxml.html.fromstring(row[4])

            # -- sitename --
            sitename = row[2]
            # -- rqmt_id --
            rqmt_id = row[1]
            # -- URL NOT NULL --
            url = row[6]
            #syokushu NOT NULL
            syokushu_Cd = row[3]
            # -- workplace pref --
            workplace_Cd = row[5]

            # -- company_name NOT NUL --
            cmpny_name = html.xpath('string(//*[@class="conditionsBox"]/*[@class="company"])').strip()
            cmpny_name = str(cmpny_name)
            # -- debuging: cmpny_name = cmpny_name[:30] if(len(cmpny_name)) > 30 else cmpny_name
            # -- title of job offer NOT NULL --
            subtitle = html.xpath('string(//*[@class="conditionsBox"]/h2)').strip()
            subtitle = self.normalize_spaces(subtitle)
            subtitle = str(subtitle)
            # -- debuging: subtitle = subtitle[:30] if(len(subtitle)) > 30 else subtitle
            # -- content --
            content = html.xpath("string(//*[@class='discriptionTbl']/tr/th[contains(text(), '仕事内容')]/following-sibling::td)").strip()
            content = self.normalize_spaces(content)
            content = str(content)
            # -- debuging: content = content[:30] if(len(content)) > 30 else content
            # -- content_wiz_tag --
            cssselectorlist = html.cssselect('table tr th:contains("仕事内容") + td')
            content_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                content_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding='utf-8').decode('utf-8')
                content_wiz_tag = self.normalize_spaces(content_wiz_tag)
                content_wiz_tag = re.sub(r'<td>','<p>',content_wiz_tag)
                content_wiz_tag = re.sub(r'</td>','</p>',content_wiz_tag)
                content_wiz_tag = str(content_wiz_tag)

            # -- workplace --
            workplace = html.xpath('string(//*[@id="mainContentsArea"]/section[1]/div[1]/div[3]/dl[2]/dd)').strip()
            #workplace = html.cssselect('.incomeLocation .location+dd').text.strip()
            workplace = self.normalize_spaces(workplace)
            workplace = str(workplace)
            # -- debuging: workplace = workplace[:30] if(len(workplace)) > 30 else workplace
            # -- skill --
            skill = html.xpath("string(//*[@class='discriptionTbl']/tr/th[contains(text(), '応募条件')]/following-sibling::td)").strip()
            skill = self.normalize_spaces(skill)
            skill = str(skill)
            # -- debuging: skill = skill[:30] if(len(skill)) > 30 else skill
            # -- skille_wiz_tag --
            cssselectorlist = html.cssselect('table tr th:contains("応募条件") + td')
            skill_wiz_tag = ""
            if(len(cssselectorlist) >= 1):
                skill_wiz_tag = lxml.html.tostring(cssselectorlist[0],encoding="utf-8").decode('utf-8')
                skill_wiz_tag = self.normalize_spaces(skill_wiz_tag)
                skill_wiz_tag = re.sub(r'<td>','<p>',skill_wiz_tag)
                skill_wiz_tag = re.sub(r'</td>','</p>',skill_wiz_tag)
                skill_wiz_tag = str(skill_wiz_tag)

            # -- payment monthly --
            payment = html.xpath('string(//*[@id="mainContentsArea"]/section[1]/div[1]/div[3]/dl[1]/dd)').strip()
            #payment = html.cssselect('.incomeLocation .income+dd').text.strip()
            payment = self.normalize_spaces(payment)
            payment = str(payment)
            # -- debuging: payment = payment[:30] if(len(payment)) > 30 else payment

            last_confirmed_at = datetime.now().strftime('20%y-%m-%d')


            item = {}
            item['sitename'] = sitename
            item['rqmt_id'] = rqmt_id
            item['cmpny_name'] = cmpny_name
            item['subtitle'] = subtitle
            item['syokushu_Cd'] = syokushu_Cd
            item['content'] = content
            item['content_wiz_tag'] = content_wiz_tag
            item['workplace_Cd'] = workplace_Cd
            item['workplace'] = workplace
            item['skill'] = skill
            item['skill_wiz_tag'] = skill_wiz_tag
            item['payment'] = payment
            item['url'] = url
            item['last_confirmed_at'] = last_confirmed_at


            self.validate(item)


            create_sql = """
            CREATE TABLE IF NOT EXISTS tj_scraping_for_mynb(
                id INTEGER NOT NULL AUTO_INCREMENT,
                rqmt_id VARCHAR(50) UNIQUE NOT NULL,
                sitename VARCHAR(255) NOT NULL,
                cmpny_name VARCHAR(255) NOT NULL,
                subtitle TEXT NOT NULL,
                syokushu_Cd VARCHAR(255) NOT NULL,
                content MEDIUMTEXT NOT NULL,
                content_wiz_tag MEDIUMTEXT NOT NULL,
                workplace_Cd VARCHAR(255) NOT NULL,
                workplace TEXT NOT NULL,
                skill TEXT NOT NULL,
                skill_wiz_tag TEXT NOT NULL,
                payment TEXT NOT NULL,
                url VARCHAR(255) NOT NULL,
                last_confirmed_at DATE NOT NULL,
                PRIMARY KEY(id)
            )DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;
            """

           # c.execute(create_sql)

            insert_sql = """
            INSERT IGNORE INTO tj_scraping_for_mynb
            (rqmt_id, sitename, cmpny_name, subtitle, syokushu_Cd, content, content_wiz_tag, workplace_Cd, workplace, skill, skill_wiz_tag, payment, url, last_confirmed_at)
            VALUES
            (%(rqmt_id)s, %(sitename)s, %(cmpny_name)s, %(subtitle)s, %(syokushu_Cd)s, %(content)s, %(content_wiz_tag)s, %(workplace_Cd)s, %(workplace)s, %(skill)s, %(skill_wiz_tag)s, %(payment)s, %(url)s, %(last_confirmed_at)s)
            """

            c.execute(insert_sql,item)
            conn.commit()


    def normalize_spaces(self, s):
        s = re.sub(r'\s+', " ", s)
        return s


    def validate(self, item):
        if(len(item['cmpny_name']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'cmpny_name is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['subtitle']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'subtitle is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['content']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'content is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['workplace']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'workplace is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['skill']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'skill is invalid'])
            #print(self.invalid_scrape_list)
        if(len(item['payment']) == 0):
            self.invalid_scrape_list.append([item['url'] , 'payment is invalid'])
            #print(self.invalid_scrape_list)




if __name__ == "__main__":
    argvs = sys.argv
    exe = ScraperTasks()
    exe.main(argvs[1])
