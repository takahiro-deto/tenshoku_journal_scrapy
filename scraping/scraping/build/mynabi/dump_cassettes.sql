SELECT DISTINCT
  CONCAT('"', id, '"') AS id,
  CONCAT('"', rqmt_id, '"') AS rqmt_id,
  CONCAT('"', sitename, '"') AS sitename,
  CONCAT('"', cmpny_name, '"') AS cmpny_name,
  CONCAT('"', subtitle, '"') AS subtitle,
  CONCAT('"', syokushu_Cd, '"') AS job_code_full,
  CONCAT('"', syokushu_Cd, '"') AS job_code_big,
  CONCAT('"', syokushu_Cd, '"') AS job_code_mid,
  CONCAT('"', content, '"') AS content,
  CONCAT('"', content_wiz_tag, '"') AS content_wiz_tag,
  CONCAT('"', workplace_Cd, '"') AS area_code,
  CONCAT('"', workplace, '"') AS workplace,
  CONCAT('"', skill, '"') AS skill,
  CONCAT('"', skill_wiz_tag, '"') AS skill_wiz_tag,
  CONCAT('"', payment, '"') AS payment,
  CONCAT('"', url, '"') AS url,
  CONCAT('"', last_confirmed_at, '"') AS last_confirmed_at
FROM tj_scraping_for_mynb;
