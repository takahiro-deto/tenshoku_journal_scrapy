-- CREATE TABLE IF NOT EXISTS tj_recruitments(
--   id INTEGER NOT NULL AUTO_INCREMENT,
--   rqmt_id VARCHAR(50) UNIQUE NOT NULL,
--   sitename VARCHAR(255) NOT NULL,
--   cmpny_name VARCHAR(255) NOT NULL,
--   subtitle TEXT NOT NULL,
--   job_code_full VARCHAR(255) NOT NULL,
--   job_code_big VARCHAR(255) NOT NULL,
--   job_code_mid VARCHAR(255) NOT NULL,
--   message MEDIUMTEXT,
--   content MEDIUMTEXT NOT NULL,
--   content_wiz_tag MEDIUMTEXT NOT NULL,
--   area_code VARCHAR(255) NOT NULL,
--   workplace TEXT NOT NULL,
--   workplace_wiz_tag TEXT NOT NULL,
--   skill TEXT NOT NULL,
--   skill_wiz_tag TEXT NOT NULL,
--   payment TEXT NOT NULL,
--   payment_wiz_tag TEXT NOT NULL,
--   url VARCHAR(255) NOT NULL,
--   expired_at DATETIME,
--   last_confirmed_at DATETIME,
--   PRIMARY KEY(id)
-- )DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

LOAD DATA LOCAL INFILE '/vagrant/scraping/scraping/build/mynabi/import_cassettes.csv' INTO TABLE tj_recruitments FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\r\n' \
IGNORE 1 LINES \
(@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17) \
SET rqmt_id=@2, sitename=@3, cmpny_name=@4, subtitle=@5, job_code_full=@6, job_code_big=@7, job_code_mid=@8, content=@9, content_wiz_tag=@10, \
area_code=@11, workplace=@12, skill=@13, skill_wiz_tag=@14, payment=@15, url=@16, last_confirmed_at=@17;
