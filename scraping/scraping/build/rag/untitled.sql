CREATE TABLE tj_recruitments (
  id INTEGER NOT NULL AUTO_INCREMENT,
  rqmt_id VARCHAR(50) NOT NULL,
  sitename VARCHAR(255) NOT NULL,
  cmpny_name VARCHAR(255) NOT NULL,
  subtitle TEXT NOT NULL,
  job_code_full VARCHAR(255) NOT NULL,
  job_code_big VARCHAR(255) NOT NULL,
  job_code_mid VARCHAR(255) NOT NULL,
  message MEDIUMTEXT DEFAULT NULL,
  content MEDIUMTEXT NOT NULL,
  content_wiz_tag MEDIUMTEXT NOT NULL,
  area_code BLOB NOT NULL,
  workplace TEXT NOT NULL,
  workplace_wiz_tag TEXT NOT NULL,
  skill TEXT NOT NULL,
  skill_wiz_tag TEXT NOT NULL,
  payment TEXT NOT NULL,
  payment_wiz_tag TEXT NOT NULL,
  url VARCHAR(255) NOT NULL,
  expired_at DATE DEFAULT NULL,
  last_confirmed_at DATE DEFAULT NULL,
  new_flag INTEGER NOT NULL DEFAULT 0,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  created_at TIMESTAMP NULL DEFAULT NULL,
  updated_at TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY(`id`),
  KEY `rqmt_id`(`rqmt_id`)
)ENGINE=Mroonga DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE tj_recruitments ADD FULLTEXT(payment);