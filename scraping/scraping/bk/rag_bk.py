from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from affsite2.items import Cassette
import re


class RagSpider(CrawlSpider):
    name = "ragbk"
    allowed_domains = ["www.r-agent.com"]

    start_urls = (
        "http://www.r-agent.com/kensaku/kinmuchi/tokyo/ocpt1-11/ocpt2-01/ocpt3-01/",
    )

    rules = (
        Rule(LinkExtractor(allow=r'/kensaku/kinmuchi/.*?\.html.*?', restrict_css=('#searchResult a'))),
        Rule(LinkExtractor(allow=r'/kensaku/kyujin/\d{8}-\d{3}-\d{2}-\d{3}\.html',restrict_css=('#searchResult a')), callback='parse_cassette', follow=False),
    )


    def parse_cassette(self, response):
        item = Cassette()

        #sitename
        item['sitename'] = "リクルートエージェント"
        #rqmt_id
        item['rqmt_id'] = ""
        #company_name NOT NULL
        item['cmpny_name'] = response.css('.syosaipagettl span ::text').extract_first()
        #title of job offer NOT NULL
        item['subtitle'] = response.css('.contentTopWrap p em::text').extract_first()
        #syokushu NOT NULL
        item['syokushu'] = response.css('.syosaipageDataTable th:contains("募集職種")+td::text').extract_first()
        #content
        item['content'] = response.css('.syosaipageH3:contains("仕事の内容")+p::text').extract_first()
        #workplace pref
        item['workplace_pref'] = response.css('.firstCell:contains("予定勤務地")+td::text').extract_first()
        #workplace
        item['workplace'] = ""
        #address
        item['address'] = ""
        #skill
        item['skill'] = response.css('.syosaipageH3:contains("必要な経験・能力等")+p::text').extract_first()
        #payment annual
        item['annual_payment'] = response.css('.firstCell:contains("想定年収")+td::text').extract_first()
        #payment monthly
        item['monthly_payment'] = ""
        #URL NOT NULL
        item['url'] = response.url

        yield item
