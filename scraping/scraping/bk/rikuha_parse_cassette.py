import scrapy
import re

from affsite2.items import Cassette

class RikuhaParseCassette(scrapy.Spider):
    name = "rikuhaParseCassette"
    allowed_domain = ["haken.rikunabi.com"]

    with open("./spiders/rikuha_url.csv") as f:
        start_urls = [url.strip() for url in f.readlines()]


    def parse(self, response):
        item = Cassette()

        #sitename
        item['sitename'] = "リクナビ派遣"
        #rqmt_id
        item['rqmt_id'] = re.search(r'work_Cd=(.*)', response.request.url).group(1)
        #company_name NOT NULL
        item['cmpny_name'] = "社名非公開"
        #title of job offer NOT NULL
        item['subtitle'] = response.css('.cst_dtl_wrap .cst_dtl_hdr_wrap .ttl_cst::text').extract_first()
        #syokushu NOT NULL
        item['syokushu'] = re.search(r'shokushu_Cd=(.*)', response.request.url).group(1)
        #content
        item['content'] = response.css('.txt_job_dtl').xpath('string()').extract_first()
        #workplace pref
        item['workplace_pref'] = ""
        #workplace
        wkplace = response.css('.tbl_cont_cst_dtl_item th:contains("勤務地")+td').xpath('string()').extract_first()
        wkplace = re.sub(r'[\r|\n|\t]+', '\t', wkplace)
        item['workplace'] = wkplace
        #address
        item['address'] = ""
        #skill
        skill = response.css('.tbl_cont_cst_dtl_item th:contains("対象となる方")+td').xpath('string()').extract_first().strip()
        skill = re.sub(r'[\r|\n|\t]+', '\t', skill)
        item['skill'] = skill
        #payment annual
        item['annual_payment'] = ""
        #payment monthly
        hourrage = response.css('.tbl_cont_cst_dtl_item th:contains("時給")+td').xpath('string()').extract_first().strip()
        hourrage = re.sub(r'[\r|\n|\t]+', '\t', hourrage)
        item['monthly_payment'] = "時給：" + hourrage
        #URL NOT NULL
        item['url'] = response.url

        yield item
