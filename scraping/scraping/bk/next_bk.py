from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from affsite2.items import Cassette
import re

class NextSpider(CrawlSpider):
    name = "nextbk"
    allowed_domains = ["next.rikunabi.com"]

    start_urls = (
        "https://next.rikunabi.com/area_wp0311000000/jb0101010101",
    )

    rules = (
        Rule(LinkExtractor(allow=r'/area_wp\d+/jb\d+/.*',restrict_css=('.rnn-pagination__page'))),
        Rule(LinkExtractor(restrict_css=('.rnn-jobOfferList .rnn-textLl')),follow=False,callback="parse_cassette"),
    )


    def parse_cassette(self,response):
        pass
