from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from affsite2.items import Cassette
import re


class HatalikeSpider(CrawlSpider):
    name = "hatalikebk"
    allowed_domains = ["www.hatalike.jp"]
    start_urls = (
        "https://www.hatalike.jp/h/r/H102010s.jsp?SJT=0101&LA=018",
    )

    rules = (
        Rule(LinkExtractor(allow=r'https:\/\/www\.hatalike\.jp\/PLA_\d+\/MA_\d+\/SA_\d+\/RQ_\d+\/$'), follow=False, callback="parse_cassette"),
        Rule(LinkExtractor(allow=r'https:\/\/www\.hatalike\.jp\/PLA_\d+\/JT_\d+\/SJT_\d+\/\d+\/$')),
    )

    def parse_cassette(self, response):
        item = Cassette()

        #sitename
        item['sitename'] = "はたらいく"
        #rqmt_id
        item['rqmt_id'] = re.search(r'RQ_(\d+)\/', response.url).group(1)
        #company_name NOT NULL
        item['cmpny_name'] = response.css('.titleAreaWrap span::text').extract_first()
        #title of job offer NOT NULL
        item['subtitle'] = response.css('.outlineCol .colLeft .em01::text').extract_first()
        #syokushu NOT NULL
        referer = response.request.headers.get('Referer', None).decode('utf-8')
        item['syokushu'] = re.search(r'SJT.(\d+)', referer).group(1)
        #content
        item['content'] = response.css('.design01 tr th:contains("職種/仕事内容")+td').xpath('string()').extract_first()
        #workplace pref
        item['workplace_pref'] = ""
        #workplace
        item['workplace'] = response.css('.design01 tr th:contains("勤務地")+td').xpath('string()').extract_first()
        #address
        item['address'] = ""
        #skill
        item['skill'] = response.css('.design01 tr th:contains("対象となる方")+td').xpath('string()').extract_first()
        #payment annual
        item['annual_payment'] = ""
        #payment monthly
        item['monthly_payment'] = response.css('.design01 tr th:contains("給与")+td').xpath('string()').extract_first()
        #URL NOT NULL
        item['url'] = response.url

        yield item
