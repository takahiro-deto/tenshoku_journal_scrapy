import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from affsite2.items import Cassette
import re


class TypeSpider(CrawlSpider):
    name = "typebk"
    allowed_domain = ["type.jp"]

    start_urls = (
        "https://type.jp/job-1/1003/6/area3-tokyo_toka/?pathway=37",
    )

    rules = (
        Rule(LinkExtractor(allow=r'/job-\d+/\d+_detail/', restrict_css=('.mod-Job-info .title')), follow=False, callback="parse_cassette"),
        Rule(LinkExtractor(restrict_css=('.mod-pagination p.next a'))),
    )

    def parse_cassette(self, response):
        if(re.match(r'.*?_message.*?', response.url)):
            offer_url = response.url + "?companyMessage=false"
            yield scrapy.Request(offer_url, self.parse_cassette_for_N5)

        item = Cassette()

        #sitename
        item['sitename'] = "@type"
        #rqmt_id
        item['rqmt_id'] = re.search(r'.*?(\d+)_detail.*?', response.url).group(1)
        #company_name NOT NULL
        item['cmpny_name'] = response.css('.corp-name a ::text').extract_first()
        #title of job offer NOT NULL
        item['subtitle'] = response.css('.mod-job-detail header h1 ::text').extract_first()
        #syokushu NOT NULL
        referer_url = response.request.headers.get('Referer', None).decode('utf-8')
        item['syokushu'] = re.search('.*?job-\d+/(\d+|\d+/\d+)/.*?',referer_url).group(1)
        #content
        item['content'] = response.css('.mod-job-content h4:contains("具体的にどんな仕事")+div p').xpath('string()').extract_first().strip()
        #workplace pref
        item['workplace_pref'] = re.search('.*?area\d+-(.*?)/.*?',referer_url).group(1)
        #workplace
        item['workplace'] = response.css('#uq-detail-location dd p').xpath('string()').extract_first().strip()
        #address
        item['address'] = ""
        #skill
        item['skill'] = response.css('#uq-detail-required dd p').xpath('string()').extract_first().strip()
        #payment annual
        item['annual_payment'] = response.css('#uq-detail-salary dd p').xpath('string()').extract_first().strip()
        #payment monthly
        item['monthly_payment'] = ""
        #URL NOT NULL
        item['url'] = response.url

        yield item


    def parse_cassette_for_N5(self, response):
        item = Cassette()

        print("")
        print("this is N5")
        print("")

        #sitename
        item['sitename'] = "@type"
        #rqmt_id
        item['rqmt_id'] = re.search(r'.*?(\d+)_detail.*?', response.url).group(1)
        #company_name NOT NULL
        item['cmpny_name'] = response.css('.corp-name a ::text').extract_first()
        #title of job offer NOT NULL
        item['subtitle'] = response.css('.mod-job-detail header h1 ::text').extract_first()
        #syokushu NOT NULL
        referer_url = response.request.headers.get('Referer', None).decode('utf-8')
        item['syokushu'] = re.search('.*?job-\d+/(\d+|\d+/\d+)/.*?',referer_url).group(1)
        #content
        item['content'] = response.css('.mod-job-content h4:contains("具体的にどんな仕事")+div p').xpath('string()').extract_first().strip()
        #workplace pref
        item['workplace_pref'] = re.search('.*?area\d+-(.*?)/.*?',referer_url).group(1)
        #workplace
        item['workplace'] = response.css('#uq-detail-location dd p').xpath('string()').extract_first().strip()
        #address
        item['address'] = ""
        #skill
        item['skill'] = response.css('#uq-detail-required dd p').xpath('string()').extract_first().strip()
        #payment annual
        item['annual_payment'] = response.css('#uq-detail-salary dd p').xpath('string()').extract_first().strip()
        #payment monthly
        item['monthly_payment'] = ""
        #URL NOT NULL
        item['url'] = response.url

        yield item
