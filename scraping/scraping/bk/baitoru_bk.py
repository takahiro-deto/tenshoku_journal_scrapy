from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from affsite2.items import Cassette
import re


class BaitoruSpider(CrawlSpider):
    name = "baitorubk"
    allowed_domains = ["www.baitoru.com"]
    start_urls = (
        "http://www.baitoru.com/shain/search?shok_cat_cd=91&shok_cd=94770&tdfk_cd=10",
    )

    rules = (
        Rule(LinkExtractor(allow=r'/shain/item/\d{4}/\d{8}/', restrict_css=('.p-item__catch a')), callback='parse_cassette', follow=False),
        Rule(LinkExtractor(allow=r'/shain/search\?page=\d+&.+?&.+?&.+?', restrict_css=('.pagination a'))),
    )


    def parse_cassette(self, response):
        item = Cassette()

        #sitename
        item['sitename'] = "バイトルNEXT"
        #rqmt_id
        item['rqmt_id'] = re.search("\d{8}", response.url).group()
        #company_name NOT NULL
        item['cmpny_name'] = response.css('.p-item__heading ::text').extract_first()
        #title of job offer NOT NULL
        item['subtitle'] = response.css('.p-item__catch--detail ::text').extract_first()
        #syokushu NOT NULL
        referer = response.request.headers.get('Referer', None).decode('utf-8')
        shok_cd = re.search(r'shok_cd=(\d+)', referer).group(1)
        item['syokushu'] = shok_cd
        #content
        item['content'] = response.css('.p-item__subhead:contains("仕事内容")+div').xpath('string()').extract_first()
        #workplace pref
        item['workplace_pref'] = ""
        #workplace
        r = response.css('.p-item__condition th:contains("勤務地")+td').xpath('string()').extract_first()
        result = re.sub(r'> 地図', '', r)
        item['workplace'] = result
        #address
        item['address'] = ""
        #skill
        item['skill'] = response.css('.p-item__condition th:contains("経験・資格")+td').xpath('string()').extract_first()
        #payment annual
        item['annual_payment'] = ""
        #payment monthly
        item['monthly_payment'] = response.css('.p-item__condition th:contains("給与")+td').xpath('string()').extract_first()
        #URL NOT NULL
        item['url'] = response.url

        yield item
