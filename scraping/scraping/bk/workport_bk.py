from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from affsite2.items import Cassette
import re


class WorkportSpider(CrawlSpider):
    name = "workportbk"
    allowed_domain = ["www.workport.co.jp"]

    start_urls = (
        "http://www.workport.co.jp/all/search/area-23/job-21729/",
    )

    rules = (
        Rule(LinkExtractor(allow=r'/details/\d+/', restrict_css=('.resltbox')), follow=False, callback="parse_cassette"),
        Rule(LinkExtractor(allow=r'/all/search/.*?/.*?/\?p=\d+', restrict_css=('.com_pager'))),
    )


    def parse_cassette(self, response):
        item = Cassette()

        #sitename
        item['sitename'] = "Workport"
        #rqmt_id
        item['rqmt_id'] = re.search('\d+', response.url).group()
        #company_name NOT NULL
        item['cmpny_name'] = response.css('#DTBX h1 ::text').extract_first()
        #title of job offer NOT NULL
        item['subtitle'] = response.css('#DTBX h1 span::text').extract_first()
        #syokushu NOT NULL
        referer_url = response.request.headers.get('Referer', None).decode('utf-8')
        item['syokushu'] = re.search('.*?job-(\d+)',referer_url).group(1)
        #content
        item['content'] = response.css('.box table tr th:contains("仕事内容")+td').xpath('string()').extract_first()
        #workplace pref
        item['workplace_pref'] = re.search('.*?area-(\d+)',referer_url).group(1)
        #workplace
        item['workplace'] = response.css('.box table tr th:contains("勤務地")+td').xpath('string()').extract_first()
        #address
        item['address'] = response.css('.box table tr th:contains("本社所在地")+td').xpath('string()').extract_first()
        #skill
        item['skill'] = response.css('.box table tr th:contains("求めるスキル")+td').xpath('string()').extract_first()
        #payment annual
        item['annual_payment'] = response.css('.box table tr th:contains("想定給与")+td').xpath('string()').extract_first()
        #payment monthly
        item['monthly_payment'] = ""
        #URL NOT NULL
        item['url'] = response.url

        yield item
