from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from affsite2.items import Cassette
import re


class MynabiSpider(CrawlSpider):
    name = "mynabibk"
    allowed_domain = ["mynavi-agent.jp"]

    start_urls = (
        "https://mynavi-agent.jp/jobsearch/result.php?jb%5B%5D=10100019&ar%5B%5D=01&ic=&kw=&kw2=",
    )

    rules = (
        Rule(LinkExtractor(allow=r'/jobsearch/\d+/', restrict_css=('.searchResultList .sdw')), follow=False, callback="parse_cassette"),
        Rule(LinkExtractor(allow=r'/jobsearch/.*?pageID=\d+', restrict_css=('.paging a'))),
    )


    def parse_cassette(self, response):
        item = Cassette()

        #sitename
        item['sitename'] = "マイナビエージェント"
        #rqmt_id
        hit = response.css('.conditionsBox .status li:contains("求人No：") ::text').extract_first()
        item['rqmt_id'] = re.search('\d+', hit).group()
        #company_name NOT NULL
        item['cmpny_name'] = response.css('.conditionsBox .company').xpath('string()').extract_first()
        #title of job offer NOT NULL
        item['subtitle'] = response.css('.conditionsBox h2').xpath('string()').extract_first()
        #syokushu NOT NULL
        referer_url = response.request.headers.get('Referer', None).decode('utf-8')
        item['syokushu'] = "default"
        #content
        item['content'] = response.css('.discriptionTbl tr th:contains("仕事内容")+td').xpath('string()').extract_first()
        #workplace pref
        item['workplace_pref'] = "default"
        #workplace
        item['workplace'] = response.css('.incomeLocation .location+dd ::text').extract_first()
        #address
        item['address'] = ""
        #skill
        item['skill'] = response.css('.discriptionTbl tr th:contains("応募条件")+td').xpath('string()').extract_first()
        #payment annual
        item['annual_payment'] = response.css('.incomeLocation .income+dd ::text').extract_first()
        #payment monthly
        item['monthly_payment'] = ""
        #URL NOT NULL
        item['url'] = response.url

        yield item
