import scrapy
import re
import csv

from scraping.items import Cassette

class RikuhaGenUrlSpider(scrapy.Spider):
    name = "rikuhaGenUrl"
    allowed_domain = ["haken.rikunabi.com"]

    start_urls = (
        "http://haken.rikunabi.com/h/r/HS1B070n.jsp?cmd=SEARCH&g=K&kinmuchi_Cd=13101&shokushu_Cd=0133",
    )

    host = "http://haken.rikunabi.com/"
    base_url = host + "h/r/HS1B280n.jsp?"

    def parse(self, response):
        #max_page = response.css('.lst_pager li:nth-last-of-type(2) a').xpath('string()').extract_first()
        max_page = 5
        current_page = 1

        while(current_page <= max_page):
            next_url = response.url + "&cmd=NEXT&pageNo=" + str(current_page)
            current_page+=1
            yield scrapy.Request(next_url, self.generate_url)


    def generate_url(self, response):
        """
        -- code値の説明 --
        target_S_Code   : 求人ごとにユニークなs_codeのIDと想定される
        return_Kbn      : 詳細不明。固定値（1)
        shokushu_Cd     : 職種コード
        cmd             : 固定値（INIT）。s_codeの実行に必要な何か
        targetPage      : 詳細不明。固定値（1)
        g               : 地域ブロックコード
        totalCount      : 検索結果総件数
        kinmuchi_Cd     : 勤務地コード
        search_Area_Kbn : 地域ブロックコード
        work_Cd         : 求人ID
        """

        g_content = response.css('.change_area a::text').extract_first()
        if g_content == "関東":
            g = "K"
        elif g_content == "関西":
            g = "W"
        elif g_content == "東海":
            g = "T"
        elif g_content == "北海道":
            g = "D"
        elif g_content == "東北":
            g = "H"
        elif g_content == "北信越":
            g = "U"
        elif g_content == "中国・四国":
            g = "N"
        elif g_content == "九州・沖縄":
            g = "Q"
        else:
            g = ""

        totalCount = response.css('.txt_page_result span::text').extract_first()

        kinmuchi_Cd = re.search(r'kinmuchi_Cd=(\d+)', response.url).group(1)
        shokushu_Cd = re.search(r'shokushu_Cd=(\d+)', response.url).group(1)


        cassette_urls = response.css('.btn_bl_wrap .btn_bl::attr("href")').re(r'javascript:submitForm2\(\'\./HS1B280n\.jsp\?cmd=INIT&.*')

        print("kinmuchi_Cd", "shokushu_Cd", "rqmt_url")

        for cassette_url in cassette_urls:
            target_S_code = re.search(r'target_S_Code=(\d+)', cassette_url).group(1)
            work_Cd = re.search(r'work_Cd=(.*)\'\)', cassette_url).group(1)

            converted_url = self.base_url + \
                            "target_S_Code=" + target_S_code + \
                            "&return_Kbn=1" + \
                            "&shokushu_Cd=" + shokushu_Cd + \
                            "&cmd=INIT" + \
                            "&targetPage=1" + \
                            "&g=" + g + \
                            "&totalCount=" + totalCount + \
                            "&kinmuchi_Cd=" + kinmuchi_Cd + \
                            "&search_Area_Kbn=" + g + \
                            "&work_Cd=" + work_Cd

            #print(kinmuchi_Cd, shokushu_Cd, converted_url)

            with open('./spiders/rikuha_url.csv', 'a', newline='') as f:
                writer = csv.writer(f,delimiter=',')
                writer.writerow([converted_url])
