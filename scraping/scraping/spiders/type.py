import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from scraping.items import Joboffer
import re


class TypeSpider(CrawlSpider):
    name = "type"
    allowed_domain = ["type.jp"]

    with open("./spiders/urllist/type.txt") as f:
        start_urls = [url.strip() for url in f.readlines()]
    #start_urls = ['https://type.jp/job-1/1001/3/area5-aichi/?pathway=37']
    rules = (
      #  Rule(LinkExtractor(allow=r'/job-\d+/\d+_detail/', restrict_css=('.mod-Job-info .title')), follow=False, callback="save_contents"),
        Rule(LinkExtractor(allow=r'/job-\d+/\d+_detail/', restrict_css=('div.mod-job-info-item header.mod-job-info-header h2.title')), follow=False, callback="save_contents"),
      #  Rule(LinkExtractor(restrict_css=('.mod-pagination p.next a'))),
        Rule(LinkExtractor(restrict_css=('div.mod-pagination p.next a'))),
    )


    def save_contents(self, response):
        referer = response.request.headers.get("Referer", None).decode('utf-8')

        if(re.match(r'.*?_message.*?', response.url)):
            offer_url = response.url.replace("_message", "_detail") + "?companyMessage=false"
            yield scrapy.Request(offer_url, self.save_contents_for_N5, meta={'referer':referer})
        else:
            item = Joboffer()

            #sitename
            item['sitename'] = "at_type"
            #rqmt_id
            item['rqmt_id'] = re.search(r'.*/(\d+)_detail.*', response.url).group(1)
            #職種CD NOT NULL
            item['syokushu_Cd'] = re.search(r'.*/(job-.*)/area.*', referer).group(1)
            #勤務地CD
            item['workplace_Cd'] = re.search(r'.*/(area\d+.*?)/.*', referer).group(1)
            #URL
            item['url'] = response.url
            #HTML
            item['content'] = response.text

            yield item


    def save_contents_for_N5(self, response):
        referer = response.meta['referer']

        item = Joboffer()

        #sitename
        item['sitename'] = "at_type"
        #rqmt_id
        item['rqmt_id'] = re.search(r'.*/(\d+)_detail.*', response.url).group(1)
        #職種CD NOT NULL
        item['syokushu_Cd'] = re.search(r'.*/(job-.*)/area.*', referer).group(1)
        #勤務地CD
        item['workplace_Cd'] = re.search(r'/(area\d+.*)/', referer).group(1)
        #URL
        item['url'] = response.url
        #HTML
        item['content'] = response.text

        yield item
