import scrapy

from scraping.items import Joboffer
import re


items_count = 0


class BaitoruSpider(scrapy.Spider):

    name = "baitoru"
    allowed_domains = ["www.baitoru.com"]

    def __init__(self, exe_f=0,*args, **kwargs):
        super(BaitoruSpider, self).__init__(*args, **kwargs)
        self.exe_f = int(exe_f)
        if self.exe_f == 0:
            print(":::Default:::")
            with open("./spiders/urllist/baitoru_00.txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]
        else:
            print(":::" + str(self.exe_f) + ":::")
            with open("./spiders/urllist/baitoru_0" + str(self.exe_f) +".txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]


#    with open("./spiders/urllist_dummy/baitoru.txt") as f:
#        start_urls = [url.strip() for url in f.readlines()]

    # with open("./spiders/urllist_dummy/baitoru.txt") as f:
    #     start_urls = [url.strip() for url in f.readlines()]

    def parse(self, response):
        # if(response.css('.search-count p span ::text').extract_first() == "0"):
        #     pass
        # else:

        # for detail_url in response.css('.p-item__catch a::attr("href")').extract():
        #     yield scrapy.Request(response.urljoin(detail_url), self.save_contents)
        for detail_url in response.css('article.list-jobListDetail div div.bg01 div.bg02 div.pt11 ul.ul01 li.li01 h3 a::attr("href")').extract():
            yield scrapy.Request(response.urljoin(detail_url), self.save_contents)

            # if response.css('.pagination .current+a'):
            #     yield scrapy.Request(response.urljoin(response.css('.pagination .current+a::attr("href")').extract_first()), self.parse)
            if response.css('div.common-globalPager div div ol li.next a'):
                yield scrapy.Request(response.urljoin(response.css('div.common-globalPager div div ol li.next a::attr("pagelinkurl")').extract_first()), self.parse)


    def save_contents(self,response):
        referer = response.request.headers.get("Referer", None).decode('utf-8')

        item = Joboffer()

        #sitename
        item['sitename'] = "baitoru_next"
        #rqmt_id
        item['rqmt_id'] = re.search(r'.*/job(\d+)/.*', response.url).group(1)
        #職種CD NOT NULL
        #勤務地CD
        if 'page' in referer:
            item['syokushu_Cd'] = re.search(r'.*/jlist/.*/(.*)/.*/shain/.*', referer).group(1)
            item['workplace_Cd'] = re.search(r'.*/jlist/(.*)/.*/.*/shain/.*', referer).group(1)
        else:
            item['syokushu_Cd'] = re.search(r'.*/jlist/.*/(.*)/shain/.*', referer).group(1)            
            item['workplace_Cd'] = re.search(r'.*/jlist/(.*)/.*/shain/.*', referer).group(1)

        #URL
        item['url'] = response.url
        #HTML
        item['content'] = response.text

        global items_count
        items_count = items_count + 1
        print(items_count)
        if items_count > 15000:
            raise scrapy.exceptions.CloseSpider('bandwidth_exceeded')

        yield item
