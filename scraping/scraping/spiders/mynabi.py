import scrapy

from scraping.items import Joboffer
import re


class MynabiSpider(scrapy.Spider):
    name = "mynabi"
    allowed_domain = ["mynavi-agent.jp"]

    start_urls = ['https://mynavi-agent.jp/jobsearch/result.php']

    #def __init__(self, exe_f=0,*args, **kwargs):
    #    super(MynabiSpider, self).__init__(*args, **kwargs)
    #    self.exe_f = int(exe_f)
    #    if self.exe_f == 0:
    #        print(":::Default:::")
    #        with open("./spiders/urllist/mynabi_00.txt") as f:
    #            self.start_urls = [url.strip() for url in f.readlines()]
    #    else:
    #        print(":::" + str(self.exe_f) + ":::")
    #        with open("./spiders/urllist/mynabi_0" + str(self.exe_f) +".txt") as f:
    #            self.start_urls = [url.strip() for url in f.readlines()]


    def parse(self, response):
        if response.css('.pageBox .number p span ::text').extract_first() == "0":
            pass
        else:
            for detail_url in response.css('.resultListBox .searchResultList .sdw a::attr("href")').extract():
                yield scrapy.Request(response.urljoin(detail_url), self.save_contents)

            if response.css('.pageBox .paging a:contains("次へ")'):
                yield scrapy.Request(response.urljoin(response.css('.pageBox .paging a:contains("次へ")::attr("href")').extract_first()), self.parse)


    def save_contents(self,response):
        referer = response.request.headers.get("Referer", None).decode('utf-8')

        item = Joboffer()

        #sitename
        item['sitename'] = "mynabi_agent"
        #rqmt_id
        item['rqmt_id'] = re.search(r'.*search/(\d+)/', response.url).group(1)
        #職種CD NOT NULL
#       tag = response.css('#mainContentsArea section.footListBox ul li:nth-child(1) dl dd p:nth-child(1) a:last-child').extract_first()
        tag = response.css("div.pankuzu.pc a[href*='/jobsearch/job3_']").extract_first()
        if(tag is not None):
#            item['syokushu_Cd'] = re.search('/jobsearch/(\d+)/result',tag).group(1)
            item['syokushu_Cd'] = re.search(r'/jobsearch/job3_(\d+)/result',tag).group(1)
        else:
            item['syokushu_Cd'] = "0000"
        #勤務地CD
        ahreflists = response.css('.icnlocl:contains("勤務地") + dd a::attr("href")').extract()
        area_codes = []
        if(ahreflists):
            for href in ahreflists:
                area_codes.append(re.search(r'/jb/in/(\d+)/',href).group(1))
            area_codes = list(set(area_codes))

            item['workplace_Cd'] = ','.join(area_codes)
        else:
            item['workplace_Cd'] = None
            
        #URL
        item['url'] = response.url
        #HTML
        item['content'] = response.text

        yield item
