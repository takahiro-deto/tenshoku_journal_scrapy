#from scrapy.spiders import CrawlSpider, Rule
#from scrapy.linkextractors import LinkExtractor
#from scraping.items import Joboffer
#import re

import scrapy
from scraping.items import Joboffer
import re

items_count = 0

class WorkportSpider(scrapy.Spider):
    name = "workport"
    allowed_domain = ["www.workport.co.jp"]

#    with open("./spiders/urllist/workport.txt") as f:
#        start_urls = [url.strip() for url in f.readlines()]

    def __init__(self, exe_f=0,*args, **kwargs):
        super(WorkportSpider, self).__init__(*args, **kwargs)
        self.exe_f = int(exe_f)
        if self.exe_f == 0:
            print(":::Default:::")
            with open("./spiders/urllist/workport_00.txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]
        else:
            print(":::" + str(self.exe_f) + ":::")
            with open("./spiders/urllist/workport_0" + str(self.exe_f) +".txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]

    def parse(self, response):
        for detail_url in response.css('section.recruit div.recruitcov h2 a::attr("href")').extract():
            yield scrapy.Request(response.urljoin(detail_url), self.save_contents)

        if response.css('div.pager.v_pc ul li.next.cont a'):
            yield scrapy.Request(response.urljoin(response.css('div.pager.v_pc ul li.next.cont a::attr("href")').extract_first()), self.parse)


    def save_contents(self, response):
        referer = response.request.headers.get("Referer", None).decode('utf-8')

        item = Joboffer()
        #sitename
        item['sitename'] = "workport"
        #rqmt_id
        item['rqmt_id'] = re.search(r'.*details/.*/(\d+)/', response.url).group(1)
        #職種CD NOT NULL
        item['syokushu_Cd'] = re.search(r'.*/(job-\d+)/', referer).group(1)
        #勤務地CD
        item['workplace_Cd'] = re.search(r'.*/(area-\d+)/.*', referer).group(1)
        #URL
        item['url'] = response.url
        #HTML
        item['content'] = response.text

        global items_count
        items_count = items_count + 1
        if items_count > 15000:
            raise scrapy.exceptions.CloseSpider('bandwidth_exceeded')
        yield item
