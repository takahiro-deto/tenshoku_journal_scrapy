import scrapy

from scraping.items import Joboffer
import re


class HatalikeSpider(scrapy.Spider):
    
    name = "hatalike"
    allowed_domains = ["www.hatalike.jp"]

    def __init__(self, exe_f=0,*args, **kwargs):
        super(HatalikeSpider, self).__init__(*args, **kwargs)
        self.exe_f = int(exe_f)
        if self.exe_f == 0:
            print(":::Default:::")
            with open("./spiders/urllist/hatalike_00.txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]
        else:
            print(":::" + str(self.exe_f) + ":::")
            with open("./spiders/urllist/hatalike_0" + str(self.exe_f) +".txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]

    def parse(self, response):
        if response.css('.noResult'):
            pass
        else:
            for detail_url in response.css('.listFrame .company a::attr("href")').extract():
                if(re.search(r'toranet',detail_url)):
                    continue
                yield scrapy.Request(response.urljoin(detail_url), self.save_contents)

            if response.css('.resultArea2 tr td a:contains("次へ")'):
                yield scrapy.Request(response.urljoin(response.css('.resultArea2 tr td a:contains("次へ")::attr("href")').extract_first()), self.parse)


    def save_contents(self,response):
        referer = response.request.headers.get("Referer", None).decode('utf-8')
        param = re.search(r'YTM_Param = {(.+)}',response.text)

        item = Joboffer()

        #sitename
        item['sitename'] = "hatalike"
        #rqmt_id
        item['rqmt_id'] = re.search(r'.*/RQ_(\d+)/$', response.url).group(1)
        #職種CD NOT NULL
        if(param):
            jobparam = param.group(1)

            syokushu_Cd = re.search(r'SJT:\'(.*?)\',',jobparam)

            if(syokushu_Cd):
                item['syokushu_Cd'] = syokushu_Cd.group(1)
            else:
                item['syokushu_Cd'] = None
        else:
            item['syokushu_Cd'] = None

        #勤務地CD
        if(param):
            areaparam = param.group(1)

            area_code = re.search(r'LA:\'(.*?)\',',areaparam)

            if(area_code):
                item['workplace_Cd'] = area_code.group(1)
            else:
                item['workplace_Cd'] = None
        else:
            item['workplace_Cd'] = None

        #URL
        item['url'] = response.url
        #HTML
        item['content'] = response.text

        yield item
