import scrapy

from scraping.items import Joboffer
import re


class RagSpider(scrapy.Spider):
    name = "rag"
    allowed_domains = ["www.r-agent.com"]

    def __init__(self, exe_f=0,*args, **kwargs):
        super(RagSpider, self).__init__(*args, **kwargs)
        self.exe_f = int(exe_f)
        if self.exe_f == 0:
            print(":::Default:::")
            with open("./spiders/urllist/rag_00.txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]
        else:
            print(":::" + str(self.exe_f) + ":::")
            with open("./spiders/urllist/rag_0" + str(self.exe_f) +".txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]

        # with open("./spiders/urllist/rag.txt") as f:
        #     start_urls = [url.strip() for url in f.readlines()]

    #start_urls = ['https://www.r-agent.com/kensaku/syokusyu/ocpt1-01/ocpt2-01/']

    def parse(self, response):        
      #  for detail_url in response.css('.jobtable h2 a::attr("href")').extract():
        for detail_url in response.css('div.p-cassetteList div.p-cassetteList__item div.p-cassetteList__body.js-accordion__desc div.p-cassetteList__action a:nth-child(1)::attr("href")').extract():
            yield scrapy.Request(response.urljoin(detail_url), self.save_contents)

      #  if response.css('.searchNav.section ul a:contains("次へ")'):
      #  if response.css('div#searchResult div.u-py--l.u-text--center:nth-child(1) ul.u-display--inlineBlock  a:contains("次へ")'):
      #      yield scrapy.Request(response.urljoin(response.css('.searchNav.section ul a:contains("次へ")::attr("href")').extract_first()), self.parse)
      #      yield scrapy.Request(response.urljoin(response.css('div#searchResult div.u-py--l.u-text--center:nth-child(1) ul.u-display--inlineBlock  a:contains("次へ")::attr("href")').extract_first()), self.parse)

    def save_contents(self, response):
        referer = response.request.headers.get("Referer", None).decode('utf-8')

        item = Joboffer()

        #sitename
        item['sitename'] = "recruit_agent"
        #rqmt_id
        item['rqmt_id'] = re.search(r'.*/(\d+-\d+-\d+-\d+).html', response.url).group(1)
        #職種CD NOT NULL        
        syokushu_Cd = re.search(r'/kensaku/syokusyu/(.*)',referer).group(1)
        syokushu_Cd = re.sub(r'index\d+\.html.*','',syokushu_Cd)
        item['syokushu_Cd'] = syokushu_Cd

        #勤務地CD
        #  hrefs = response.css('h2:contains("この求人に似た求人を探す") + table th:contains("勤務地") + td ul li a::attr("href")').extract()
        hrefs = response.css('div#mkto-rtp-similar_company_kyujin table tbody tr:nth-child(2) td ul li a:nth-child(3)::attr("href")').extract()
        hreflist = []
        for href in hrefs:
            if(re.search(r'kanto|tohoku|kansai|tokai|chushikoku|kyushu|kaigai',href)):
                continue
            if(re.search(r'/kensaku/kinmuchi/(\w+)/',href)):
                hreflist.append(re.search(r'/kensaku/kinmuchi/(\w+)/',href).group(1))

        item['workplace_Cd'] = ','.join(hreflist)
        #URL
        item['url'] = response.url
        #HTML
        item['content'] = response.text

        yield item
