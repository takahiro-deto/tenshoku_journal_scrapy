import scrapy

from scraping.items import Joboffer
import re


class ToraSpider(scrapy.Spider):

    name = "tora"
    allowed_domain = ["toranet.jp"]    
    #with open("./spiders/urllist/tora.txt") as f:
    #    start_urls = [url.strip() for url in f.readlines()]

    # start_urls = [
    #      "https://toranet.jp/t/r/T102010s.jsp?ecd=01"
    #      ,"https://toranet.jp/t/r/T102010s.jsp?ecd=02"
    #      ,"https://toranet.jp/t/r/T102010s.jsp?ecd=03"
    #      ,"https://toranet.jp/t/r/T102010s.jsp?ecd=04"
    #      ,"https://toranet.jp/t/r/T102010s.jsp?ecd=05"
    #      ,"https://toranet.jp/t/r/T102010s.jsp?ecd=06"
    #      ,"https://toranet.jp/t/r/T102010s.jsp?ecd=07"
    #      ,"https://toranet.jp/t/r/T102010s.jsp?ecd=08"
    # ]
    # #start_urls = [
    # #     "https://toranet.jp/t/r/T102010s.jsp?ecd=08&index=20"
    # #]

    def __init__(self, exe_f=0,*args, **kwargs):
        super(ToraSpider, self).__init__(*args, **kwargs)
        self.exe_f = int(exe_f)
        if self.exe_f == 0:
            print(":::Default:::")
            with open("./spiders/urllist/tora_00.txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]
        else:
            print(":::" + str(self.exe_f) + ":::")
            with open("./spiders/urllist/tora_0" + str(self.exe_f) +".txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]


    def parse(self, response):
        #print(response.css('#header > div:nth-child(2) > div.hdr_left_wrap > div > span.area > span').extract())
        print(response.css('#leftCol > h2 > strong > span').extract())
        print(response.css('#header > div:nth-child(2) > div.hdr_right_wrap.cf > div > p > span.num').extract())
        for detail_url in response.css('div.lst_cst_wrap a.lnk_cst').extract():
            if(re.search(r'rel="nofollow',detail_url)):
                continue
            else:
                url = re.search(r'<a href="(.*?)" class.*?">',detail_url).group(1)
                yield scrapy.Request(url,self.save_contents)        

        if response.css('#leftCol > div.list_paging > div.pagination > p > a.next'):
            yield scrapy.Request(response.urljoin(response.css('#leftCol > div.list_paging > div.pagination > p > a.next::attr("href")').extract_first()), self.parse)


    def save_contents(self,response):

        referer = response.request.headers.get("Referer", None).decode('utf-8')
        param = re.search(r'YTM_Param = {(.+)}',response.text)

        item = Joboffer()

        #sitename
        item['sitename'] = "torabayu"
        #rqmt_id
        item['rqmt_id'] = re.search(r'rqmtId=(\d+)',response.css('meta[name=PageID]::attr("content")').extract_first()).group(1)
        #職種CD NOT NULL
        #item['syokushu_Cd'] = re.search(r'(.*jbTypeCd=|.*jb_)(\d+).*', response.url).group(2)
        item['syokushu_Cd'] = re.search(r'jbTypeCd=(\d+)',response.css('meta[name=PageID]::attr("content")').extract_first()).group(1)

        #勤務地CD
        # item['workplace_Cd'] = re.search(r'(.*srchLAreaCd=|.*la_)(\d+).*', response.url).group(2)
        item['workplace_Cd'] = re.search(r'srchLAreaCd=(\d+)',response.css('meta[name=PageID]::attr("content")').extract_first()).group(1)

        # if(param):
        #     areaparam = param.group(1)

        #     area_code = re.search(r'LA:\'(.*?)\',',areaparam)

        #     if(area_code):
        #         item['workplace_Cd'] = area_code.group(1)
        #     else:
        #         item['workplace_Cd'] = None
        # else:
        #     item['workplace_Cd'] = None
            
        #URL
        item['url'] = response.url
        #HTML
        item['content'] = response.text

        yield item