import scrapy
import re

from scraping.items import Joboffer

items_count = 0

class RikuhaSpider(scrapy.Spider):
    name = "rikuha"
    allowed_domain = ["haken.rikunabi.com"]

    totalPagingCounts = 0

#    with open("./spiders/urllist/rikuha_url.csv") as f:
#        start_urls = [url.strip() for url in f.readlines()]
#    with open("./spiders/urllist/rikuha.txt") as f:
#        start_urls = [url.strip() for url in f.readlines()]
#    start_urls = ['https://haken.rikunabi.com/h/r/HS1B070n.jsp?g=K&shokushu_Cd=0133&kinmuchi_Cd=13101&targetPage=1&search_Area_Kbn=K&return_Kbn=1&cmd=PREV&pageNo=0']


    def __init__(self, exe_f=0,*args, **kwargs):
        super(RikuhaSpider, self).__init__(*args, **kwargs)
        self.exe_f = int(exe_f)
        if self.exe_f == 0:
            print(":::Default:::")
            with open("./spiders/urllist/rikuha_00.txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]
        else:
            print(":::" + str(self.exe_f) + ":::")
            with open("./spiders/urllist/rikuha_0" + str(self.exe_f) +".txt") as f:
                self.start_urls = [url.strip() for url in f.readlines()]

    def parse(self, response):
        for detail_url in response.css('ul.lst_cst li a.lnk_cst::attr("href")').extract():
            yield scrapy.Request(
                response.urljoin(
                    detail_url.replace("javascript:submitForm2('","").replace("')","")
                    ), self.save_contents
                )
            # Next Step Count
            if response.css('form.jsc_form_resrch div.wrapper.cf div.main_2col div:nth-child(4) div.pager_wrap ul li.lnk_next_wrap a'):

                ## Reset URL without "pageNo"
                _requestPath = response.request.url.split("?")[0] + "?"
                _requestUrQuery = response.request.url.split("?")[1]

                _requestUrlPrams = _requestUrQuery.split("&")
                _arrRequestUrlParams = []

                _nextPageNo = ""
                for _requestPram in _requestUrlPrams :                
                    if _requestPram.split("=")[0] == "pageNo" :
                        _nextPageNo = "&pageNo=" + str(int(_requestPram.split("=")[1]) + 1)
                    else :
                        _arrRequestUrlParams.append(_requestPram)

                _nextPageUrl = _requestPath + "&".join(_arrRequestUrlParams) + _nextPageNo 
                yield scrapy.Request(_nextPageUrl, self.parse)


    def save_contents(self,response):

        referer = response.request.headers.get("Referer", None).decode('utf-8')

        item = Joboffer()

        #sitename
        item['sitename'] = "rikuha"

        curr_params = response.url.split("?")[1].split("&")
        for _curr_pair in curr_params :

            #rqmt_id
            if _curr_pair.split("=")[0] == "work_Cd" :
                item['rqmt_id'] = _curr_pair.split("=")[1]

        ref_params = referer.split("?")[1].split("&")
        for _param_pair in ref_params :

            # 職種CD
            if _param_pair.split("=")[0] == "shokushu_Cd" :
                item['syokushu_Cd'] = _param_pair.split("=")[1]

            # 勤務地CD
            if _param_pair.split("=")[0] == "kinmuchi_Cd" :
                item['workplace_Cd'] = _param_pair.split("=")[1]

        print("rqmt_id : " + item['rqmt_id'])
        print("syokushu_Cd : " + item['syokushu_Cd'])
        print("workplace_Cd : " + item['workplace_Cd'])

        #URL
        item['url'] = response.url
        print("url : " + item['url'])

        #HTML
        item['content'] = response.text

        global items_count
        items_count = items_count + 1
        if items_count > 15000:
            raise scrapy.exceptions.CloseSpider('bandwidth_exceeded')
        yield item
