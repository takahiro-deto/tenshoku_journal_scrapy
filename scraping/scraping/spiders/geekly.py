import scrapy
import re


class GeeklySpider(scrapy.Spider):
    name = "geekly"
    allowed_domains = ["www.geekly.co.jp"]

    start_urls = (
        "https://www.geekly.co.jp/search/",
    )

    syokusyu_list = [
        "web系SE・PG（自社製品）",
        "web系SE・PG（SI・受託）",
        "スマートフォン/モバイル系SE・PG",
        "オープン系SE・PG（自社製品）",
        "オープン系SE・PG（SI・受託）",
        "データベースエンジニア",
        "ERP・CRM・SCMエンジニア",
        "セキュリティエンジニア",
        "AI・機械学習エンジニア",
        "プロジェクトマネージャー（web系）",
        "プロジェクトマネージャー（オープン・業務）",
        "システムコンサルタント",
        "ネットワークエンジニア",
        "サーバーエンジニア",
        "ネットワーク保守運用・監視",
        "社内SE（開発）",
        "社内SE(ネットワーク)",
        "テクニカルサポート、ヘルプデスク",
        "インストラクター・技術教育",
        "テクニカルライター、技術翻訳",
        "テスティング、ローカライズ、品質管理エンジニア",
        "データマイニング",
        "webデザイナ（自社サービス）",
        "webデザイナー（受託）",
        "モバイル/ソーシャル/スマートフォンデザイナー",
        "webプロデューサー・ディレクター（自社サービス）",
        "webプロデューサ/ディレクター（受託）",
        "モバイル/ソーシャル/スマートフォンプロデューサー",
        "コーダー・マークアップエンジニア",
        "ｗeb編集・webマスター・コンテンツ企画",
        "ゲームプログラマ（ソーシャル・オンライン）",
        "ゲームプログラマ（コンシューマー）",
        "ゲームデザイナー（ソーシャル・オンライン）",
        "ゲームデザイナー（コンシューマー）",
        "イラストレーター・CG・グラフィックデザイナー",
        "ゲームプランナー/ディレクター（ソーシャル）",
        "ゲームプランナー/ディレクター（コンシューマー）",
        "サウンドクリエーター",
        "デバッカー",
        "IT業界営業",
        "ネット広告営業",
        "システム営業（ソフトウェア・受託）",
        "プリセールス・セールスエンジニア",
        "その他営業",
        "ネットマーケティング・ネットリサーチャー",
        "経理・財務",
        "人事・総務",
        "法務・知財",
        "広報・IR",
        "秘書・アドミ",
        "新規事業立ち上げ・エグゼクティブ",
        "経営企画",
        "マーケティング・商品企画",
        "広報・宣伝・IR",
        "制御系・組み込み・ファームウェア開発",
        "画像、通信系ソフト開発",
        "CAD/CAM/CAE/CIM",
    ]


    formdata = {
        "free_word": "",
        "data[nensyu]": "0",
        "data[syokusyu_id][][]": "",
    }

    def parse(self, response):
        for s in self.syokusyu_list:
            self.formdata["data[syokusyu_id][][]"] = s
            return scrapy.FormRequest(
                url="https://www.geekly.co.jp/search/job_list/",
                formdata=self.formdata,
                callback=self.parse_result
            )


    def parse_result(self, response):
        result = response.css('.nowshow .count ::text').extract_first()
        print(result)
