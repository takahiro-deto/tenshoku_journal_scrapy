import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from scraping.items import Joboffer
import re

class NextSpider(CrawlSpider):
    name = "next"
    allowed_domains = ["next.rikunabi.com"]

    start_urls = [
         'http://next.rikunabi.com/lst/'
    ]

    rules = (
        Rule(LinkExtractor(allow=r'/lst/crn\d+.*',restrict_css=('.rnn-pagination__page'))),
        Rule(LinkExtractor(allow=r'/company/.*', restrict_css=('.rnn-jobOfferList .rnn-textLl')),follow=False,callback="save_contents"),
    )

    # def __init__(self, exe_f=0,*args, **kwargs):
    #     super(NextSpider, self).__init__(*args, **kwargs)
    #     self.exe_f = int(exe_f)
    #     if self.exe_f == 0:
    #         print(":::Default:::")
    #         with open("./spiders/urllist/rnn_00.txt") as f:
    #             self.start_urls = [url.strip() for url in f.readlines()]
    #     else:
    #         print(":::" + str(self.exe_f) + ":::")
    #         with open("./spiders/urllist/rnn_0" + str(self.exe_f) +".txt") as f:
    #             self.start_urls = [url.strip() for url in f.readlines()]

    # def parse(self, response):
    #     for detail_url in response.css('.rnn-jobOfferList .rnn-textLl').extract():
    #         yield scrapy.Request(response.urljoin(detail_url), self.save_contents)

    #     if response.css('.rnn-pagination__next a'):
    #         yield scrapy.Request(response.urljoin(response.css('.rnn-pagination__next a::attr("href")').extract_first()), self.parse)


    def save_contents(self,response):
        referer = response.request.headers.get("Referer", None).decode('utf-8')

        if(re.match(r'.*n_ichiran_cst_n5_ttl.*', response.url)):
            newurl = response.url.replace("nx1", "nx2")
            yield scrapy.Request(response.urljoin(newurl), self.save_contents_for_N5, meta={'referer':referer})
        else:
            item = Joboffer()

            #sitename
            item['sitename'] = "rikunabi_next"

            #rqmt_id
            item['rqmt_id'] = re.search(r'.*_rq(\d+).*', response.url).group(1)

            #職種CD NOT NULL
#           job_code = response.css('body > div:nth-child(7) > div.rnn-stage.rnn-otherJobOfferArea > div > div > div > table > tr:nth-child(2) > td > p > a:last-child').extract_first()
            job_code = response.css('ul.rnn-breadcrumbs li:nth-child(4)').extract_first()
            job_code = re.search(r'lst_jb(\d+)/',job_code).group(1)
            item['syokushu_Cd'] = job_code

            #勤務地CD
            ahreflists = response.css('.rnn-tableGrid .rnn-col-10 a[href*=wp]::attr(href)').extract()
            workplace_Cd_list = []

            for href in ahreflists:
                workplace_Cd_list.append(re.search(r'/area_wp(\d+)/',href).group(1))
            workplace_Cd_list = list(set(workplace_Cd_list))            

            item['workplace_Cd'] = ','.join(workplace_Cd_list)

            #URL
            item['url'] = response.url

            #HTML
            item['content'] = response.text

            yield item

    def save_contents_for_N5(self,response):
        referer = response.meta['referer']
        item = Joboffer()

        #sitename
        item['sitename'] = "rikunabi_next"

        #rqmt_id
        item['rqmt_id'] = re.search(r'.*_rq(\d+).*', response.url).group(1)

        #職種CD NOT NULL
#       job_code = response.css('body > div:nth-child(7) > div.rnn-stage.rnn-otherJobOfferArea > div > div > div > table > tr:nth-child(2) > td > p > a:last-child').extract_first()
        job_code = response.css('ul.rnn-breadcrumbs li:nth-child(4)').extract_first()
        job_code = re.search(r'lst_jb(\d+)/',job_code).group(1)
        item['syokushu_Cd'] = job_code

        #勤務地CD
        ahreflists = response.css('.rnn-tableGrid .rnn-col-10 a[href*=wp]::attr(href)').extract()
        workplace_Cd_list = []

        for href in ahreflists:
            workplace_Cd_list.append(re.search(r'/area_wp(\d+)/',href).group(1))
        workplace_Cd_list = list(set(workplace_Cd_list))            

        item['workplace_Cd'] = ','.join(workplace_Cd_list)

        #URL
        item['url'] = response.url

        #HTML
        item['content'] = response.text

        yield item
