# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import mysql.connector
#from redis import Redis
#from rq import Queue
import warnings

class ValidationPipeline(object):
    """
    Item Validation
    """
    def process_item(self, item, spider):
        if not item['sitename']:
            raise DropItem('missing sitename')
        if not item['rqmt_id']:
            raise DropItem('missing rqmt_id')
        if not item['syokushu_Cd']:
            raise DropItem('missing syokushu_Cd')
        if not item['workplace_Cd']:
            raise DropItem('missing workplace_Cd')

        return item


class MySQLPipeline(object):
    """
    MySQL Validation
    """

    def open_spider(self, spider):
        settings = spider.settings
        params = {
            'host' : settings.get('MYSQL_HOST', 'localhost'),
            'db' : settings.get('MYSQL_DATABASE', 'journal'),
            'user' : settings.get('MYSQL_USER', 'journal'),
            'passwd' : settings.get('MYSQL_PASSWORD', 'awE_M1Y@Xp>xUW(s'),
            'charset' : settings.get('MYSQL_CHARSET', 'utf8'),
        }

        warnings.filterwarnings('ignore')

        self.conn = mysql.connector.connect(**params)
        self.c = self.conn.cursor()

    #    self.c.execute('''
    #        CREATE TABLE IF NOT EXISTS joboffers(
    #            id INTEGER NOT NULL AUTO_INCREMENT,
    #            rqmt_id VARCHAR(50) UNIQUE NOT NULL,
    #            sitename VARCHAR(255) NOT NULL,
    #            syokushu_Cd TEXT NOT NULL,
    #            content LONGTEXT,
    #            workplace_Cd TEXT,
    #            url VARCHAR(255) NOT NULL,
    #            PRIMARY KEY(id)
    #        )
    #    ''')

    #    self.conn.commit()

    def close_spider(self, spider):
        self.conn.close()



    def process_item(self, item, spider):
        sqlnext = """
        INSERT IGNORE INTO joboffers
            (sitename, rqmt_id, syokushu_Cd, content, workplace_Cd ,url)
        VALUES
            (%(sitename)s, %(rqmt_id)s, %(syokushu_Cd)s, %(content)s, %(workplace_Cd)s, %(url)s)
        """

        self.c.execute(sqlnext, dict(item))

        self.conn.commit()
        return item


# class RQPipeline(object):
#     """
#     RQにジョブを投入するPipeline
#     """

#     def open_spider(self, spider):
#         """
#         Spiderの開始時にRedisに接続してQueueオブジェクトを取得する
#         """
#         self.queue = Queue(connection=Redis())


#     def process_item(self, item, spider):
#         """
#         RQにジョブを投入する
#         """
#         self.queue.enqueue('scraper_tasks.scrape', item['rqmt_id'], return_ttl=0)
#         return item
