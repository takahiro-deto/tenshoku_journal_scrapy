#!/usr/bin/bash
#!/usr/bin/bash
#PATH
PATH=/home/hello_gdesignlab2/.pyenv/shims:
PATH=$PATH/home/hello_gdesignlab2/.pyenv/bin:
PATH=$PATH/usr/local/bin:/usr/bin:
PATH=$PATH/usr/local/sbin:
PATH=$PATH/usr/sbin:
PATH=$PATH/home/hello_gdesignlab2/.local/bin:
PATH=$PATH/home/hello_gdesignlab2/bin

#DIR
HOME=/home/hello_gdesignlab2
APP_HOME=$HOME/app/tenshoku_journal_scrapy-master
APP_SPIDER_HOME=$APP_HOME/scraping/scraping
APP_BUILD_HOME=$APP_SPIDER_HOME/build
APP_BIN_DIR=$APP_HOME/bin

#APP_DATE
DATE=`date '+%Y_%m_%d_%H%M%S'`

#GCS_PATH_ROOT
GCS_SCRAPY_ROOT=gs://scraping-result-warehouse

#GCS_PATH_APP
SCRAPY_EXEC_DATE=`date --date "9 hours" '+%F'`

#SPIDER_LOG_HOME
SPIDER_LOG_HOME=$APP_SPIDER_HOME/log

#move build home
cd $APP_BUILD_HOME/$1

# Execute if all .fin Files are exist
FIN_FILE_COUNTS=`ls *.fin | wc -l`  

## Set Max_fin_files
if [ $1 = "baitoru" ] ; then
	MAX_FIN_FILES=4
elif [ $1 = "hatalike" ] ; then
	MAX_FIN_FILES=2
elif [ $1 = "mynabi" ] ; then
	MAX_FIN_FILES=2
elif [ $1 = "next" ] ; then
	MAX_FIN_FILES=1
elif [ $1 = "rag" ] ; then
	MAX_FIN_FILES=1
elif [ $1 = "rikuha" ] ; then
	MAX_FIN_FILES=3
elif [ $1 = "tora" ] ; then
	MAX_FIN_FILES=1
elif [ $1 = "type" ] ; then
	MAX_FIN_FILES=1
elif [ $1 = "workport" ] ; then
	MAX_FIN_FILES=4
else
	MAX_FIN_FILES=99
fi	

if [ $FIN_FILE_COUNTS != $MAX_FIN_FILES ] ; then

	  FIN_FILE_COUNTS=`ls *.fin | wc -l`

else

	echo "***************START BUILD***************"
	# 1. scraping base data
	echo -e "# 1. Scraping crawled data..."
	python scraper_tasks.py $1

	# 2. dump sqlfile and convert into csv
	echo -e "\n\n"
	echo -e "# 2. Dump SQLfile and convert into CSV..."

	mysql -u journal -p'awE_M1Y@Xp>xUW(s' -h localhost journal -e "`cat dump_cassettes.sql`"  > ./dump_cassettes.csv

	# 3. translate code_list
	echo -e "\n\n"
	echo -e "# 3. Translate CODE_LIST..."
	python translate.py

	# 4. import to  mariadb
	#echo -e "\n\n"
	#echo -e "# 4. Importing to MariaDB..."
	#mysql -u journal -p'awE_M1Y@Xp>xUW(s' --local_infile=1 journal -e "`cat import_mariadb.sql`"

	# 4.COPY TO GCS

	# 4.1 Move to Backup
	gsutil mv $GCS_SCRAPY_ROOT/$1/dump_cassettes.csv $GCS_SCRAPY_ROOT/$1/bk/dump_cassettes_$SCRAPY_EXEC_DATE.csv
        gsutil mv $GCS_SCRAPY_ROOT/$1/import_cassettes.csv $GCS_SCRAPY_ROOT/$1/bk/import_cassettes_$SCRAPY_EXEC_DATE.csv

	gsutil cp dump_cassettes.csv $GCS_SCRAPY_ROOT/$1/dump_cassettes.csv
	gsutil cp import_cassettes.csv $GCS_SCRAPY_ROOT/$1/import_cassettes.csv

	#temporary to S3
	aws s3 cp dump_cassettes.csv s3://deto/crawl_backup/$1/dump_cassettes_$SCRAPY_EXEC_DATE.csv
	aws s3 cp import_cassettes.csv s3://deto/crawl_backup/$1/import_cassettes_$SCRAPY_EXEC_DATE.csv

	# 5. remove unnecessary file
	echo -e "\n\n"
	echo -e "# 5. Removing unnecessary file..."
	DATE=`date --date "9 hours" '+%Y_%m_%d_%H%M%S'`
	rm dump_cassettes.csv
	rm import_cassettes.csv
	rm invalid_scrape_list_*

	# 6. truncate MySQL
	cd $APP_BIN_DIR
	#mysql -u journal -p'awE_M1Y@Xp>xUW(s' -h localhost journal -e "delete from joboffers where sitename='$1';"
	#mysql -u journal -p'awE_M1Y@Xp>xUW(s' -h localhost journal -e "`cat truncate_scrape_data_$1.sql`"

	# 7.Remove .fin Files
	cd $APP_BUILD_HOME/$1
	rm *.fin

	echo -e "\n\n"
	echo "*************FINISHED BUILD!*********"
fi


