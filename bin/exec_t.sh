#!/usr/bin/bash
#PATH
PATH=/home/hello_gdesignlab2/.pyenv/shims:
PATH=$PATH/home/hello_gdesignlab2/.pyenv/bin:
PATH=$PATH/usr/local/bin:/usr/bin:
PATH=$PATH/usr/local/sbin:
PATH=$PATH/usr/sbin:
PATH=$PATH/home/hello_gdesignlab2/.local/bin:
PATH=$PATH/home/hello_gdesignlab2/bin

#DIR
HOME=/home/hello_gdesignlab2
APP_HOME=$HOME/app/tenshoku_journal_scrapy-master
APP_SPIDER_HOME=$APP_HOME/scraping/scraping
APP_BUILD_HOME=$APP_SPIDER_HOME/build
APP_BIN_DIR=$APP_HOME/bin

#APP_DATE
DATE=`date '+%Y_%m_%d_%H%M%S'`

#GCS_PATH_ROOT
GCS_SCRAPY_ROOT=gs://scraping-result-warehouse

#GCS_PATH_APP
SCRAPY_EXEC_DATE=`date --date "9 hours" '+%F'`

#SPIDER_LOG_HOME
SPIDER_LOG_HOME=$APP_SPIDER_HOME/log


#move scrapy app dir
cd $APP_SPIDER_HOME

#exec scrapy crawl
#$2 for baitoru
echo `pwd`
echo "DO_SCRAPY_CWARL"
if [ $1 = "baitoru" ] ; then
echo "a"
#scrapy crawl $1 -a exe_f=$2 > $SPIDER_LOG_HOME/$1/scrapy_log_$2_$SCRAPY_EXEC_DATE.log
elif [ $1 = "rikuha" ] ; then
#scrapy crawl rikuhaSaveHtml -a exe_f=$2 > $SPIDER_LOG_HOME/$1/scrapy_log_$2_$SCRAPY_EXEC_DATE.log
echo "d"
else
echo "c"
#scrapy crawl $1 > $SPIDER_LOG_HOME/$1/scrapy_log_$SCRAPY_EXEC_DATE.log
fi

#move build home
cd $APP_BUILD_HOME/$1
echo "DO_BUILD"
echo `pwd`

# Execute build Task 
if [ $1 = "baitoru" ] ; then
# make fin file as paramters
touch $1_$2.fin
else
#exec build
#sh ./build.sh
echo "***************START BUILD***************"
# 1. scraping base data
echo -e "# 1. Scraping crawled data..."
python scraper_tasks.py $1

# 2. dump sqlfile and convert into csv
echo -e "\n\n"
echo -e "# 2. Dump SQLfile and convert into CSV..."
#mysql -u tj_user_0001 -p'awE_M1Y@Xp>xUW(s' -h localhost tenshoku_journal \
#-e "`cat dump_cassettes.sql`" | sed -e 's/\t/,/g' > /vagrant/scraping/scraping/build/baitoru/dump_cassettes.csv

mysql -u journal -p'awE_M1Y@Xp>xUW(s' -h localhost journal -e "`cat dump_cassettes.sql`"  > ./dump_cassettes.csv

# 3. translate code_list
echo -e "\n\n"
echo -e "# 3. Translate CODE_LIST..."
python translate.py

# 4. import to  mariadb
#echo -e "\n\n"
#echo -e "# 4. Importing to MariaDB..."
#mysql -u journal -p'awE_M1Y@Xp>xUW(s' --local_infile=1 journal -e "`cat import_mariadb.sql`"

# 4.COPY TO GCS
echo "gsutil cp dump_cassettes.csv $GCS_SCRAPY_ROOT/$1/dump_cassettes_$SCRAPY_EXEC_DATE.csv"
gsutil cp dump_cassettes.csv $GCS_SCRAPY_ROOT/$1/dump_cassettes_$SCRAPY_EXEC_DATE.csv

echo "gsutil cp dump_cassettes.csv $GCS_SCRAPY_ROOT/$1/import_cassettes_$SCRAPY_EXEC_DATE.csv "
gsutil cp dump_cassettes.csv $GCS_SCRAPY_ROOT/$1/import_cassettes_$SCRAPY_EXEC_DATE.csv

# 4.temporary to S3
echo "aws s3 cp dump_cassettes.csv s3://deto/crawl_backup/$1/dump_cassettes_$SCRAPY_EXEC_DATE.csv"
aws s3 cp dump_cassettes.csv s3://deto/crawl_backup/$1/dump_cassettes_$SCRAPY_EXEC_DATE.csv

echo "aws s3 cp dump_cassettes.csv s3://deto/crawl_backup/$1/import_cassettes_$SCRAPY_EXEC_DATE.csv"
aws s3 cp dump_cassettes.csv s3://deto/crawl_backup/$1/import_cassettes_$SCRAPY_EXEC_DATE.csv

# 5. remove unnecessary file
echo -e "\n\n"
echo -e "# 5. Removing unnecessary file..."
DATE=`date --date "9 hours" '+%Y_%m_%d_%H%M%S'`
mv dump_cassettes.csv log/dump_cassettes_$DATE.csv
mv import_cassettes.csv log/import_cassettes_$DATE.csv
mv invalid_scrape_list_* log/

# 6. truncate MySQL
cd $APP_BIN_DIR
#mysql -u journal -p'awE_M1Y@Xp>xUW(s' -h localhost journal -e "delete from joboffers where sitename='$1';"
mysql -u journal -p'awE_M1Y@Xp>xUW(s' -h localhost journal -e "`cat truncate_scrape_data_$1.sql`"

echo -e "\n\n"
echo "*************FINISHED BUILD!*************"

fi
